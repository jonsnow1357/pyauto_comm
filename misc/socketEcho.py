#!/usr/bin/env python
# template: app_simple
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

import argparse
import socket

def mainApp():
  print("[{}] listening on port {:d}".format(modName, cliArgs["port"]))

  with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind(("localhost", cliArgs["port"]))
    s.listen(1)

    while (True):
      conn, addr = s.accept()
      print("open connection from {}".format(addr))
      while (True):
        data = conn.recv(32)
        if not data:
          break
        print("data:", data)
        conn.send(data)  # echo
      print("close connection to {}".format(addr))
      conn.close()

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  appDesc = "telnet (echo) server"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-p", "--port", type=int, default=9999, help="port to listen to")

  cliArgs = vars(parser.parse_args())
  #print("[{}] {}".format(modName, cliArgs))

  #parser.print_help()
  mainApp()
