#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_comm.notification as libNotify

def mainApp():
  #libNotify.disableCertVerification()
  serv = libNotify.Twitter()

  if (cliArgs["action"] == "cfg"):
    logger.info(serv)
  elif (cliArgs["action"] == "user"):
    serv.getUserInfo()
    serv.showUserInfo()
  elif (cliArgs["action"] == "activity"):
    serv.getTimeline()
  if (cliArgs["action"] == "delete"):
    while (True):
      lstId = serv.getTimeline()
      if (len(lstId) == 0):
        break
      for v in lstId:
        serv.delMessage(v, False)
  elif (cliArgs["action"] == "send"):
    #if(os.path.isfile(cliArgs["file"])):
    #  with open(cliArgs["file"]) as fIn:
    #    for ln in fIn:
    #      serv.sendMessage(ln.strip(" \n\r"))
    #      time.sleep(serv.retryTimeout)
    if (len(cliArgs["msg"]) > 0):
      serv.sendMessage(cliArgs["msg"])
    else:
      serv.sendMessage("In the face of ambiguity, refuse the temptation to guess.")

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "microblogging"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action",
                      help="action",
                      choices=("cfg", "user", "activity", "delete", "send"))
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-m", "--msg", default="", help="message")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
