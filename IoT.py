#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import requests
import packaging.version

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_base.config
import pyauto_comm.web as libWeb

def enableCertVerification():
  libWeb.enableCertVerification()

def disableCertVerification():
  libWeb.disableCertVerification()

class MQTTDataService(libWeb.WebService):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(MQTTDataService, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)

    import paho.mqtt
    import paho.mqtt.client as mqtt_client

    ver = packaging.version.parse(paho.mqtt.__version__)
    if (ver.major == 1):
      self._conn = mqtt_client.Client()
    else:
      self._conn = mqtt_client.Client(mqtt_client.CallbackAPIVersion.VERSION1)
    #self._conn._connect_timeout = 1.0
    self._conn.username_pw_set(self._auth["user"], password=self._auth["pass"])
    self._conn.on_connect = self._on_connect
    self._conn.on_disconnect = self._on_disconnect
    self._conn.on_subscribe = self._on_subscribe
    self._conn.on_message = self._on_message
    self._conn.on_publish = self._on_publish
    #self._conn.tls_insecure_set(True)
    self.connected = False

  def _on_connect(self, client, userdata, flags, rc):
    import paho.mqtt.client as mqtt_client
    logger.info("connected: {}, {}".format(mqtt_client.connack_string(rc), flags))
    self.connected = True

  def _on_disconnect(self, client, userdata, rc):
    import paho.mqtt.client as mqtt_client
    logger.info("disconnected: {}".format(mqtt_client.connack_string(rc)))
    self.connected = False

  def _on_subscribe(self, client, userdata, mid, granted_qos):
    logger.info("subscribe: {} {}".format(mid, granted_qos))

  def _on_message(self, client, userdata, message):
    logger.info("message: '{}'".format(message))

  def _on_publish(self, client, userdata, mid):
    logger.info("publish: '{}'".format(mid))

  def connect(self, **kwargs):
    if (self.connected):
      return

    #self._conn.reinitialise()
    self._conn.connect(host=self._endpoint["url"], port=1883)
    self._conn.loop_start()
    while (not self.connected):
      pass

  def disconnect(self, **kwargs):
    self._conn.loop_stop()
    self._conn.disconnect()

  def send(self, **kwargs):
    raise NotImplementedError

class ThingSpeakChannel(object):

  def __init__(self):
    self._id = None
    self.name = None
    self.description = None
    self.public = False
    self.fields = []
    self.api_key_rd = None
    self.api_key_wr = None

  def __str__(self):
    return "{}: {} (id: {}), public: {}, {:d} field(s)".format(
        self.__class__.__name__, self.name, self._id, "[x]" if (self.public) else "[ ]",
        len(self.fields))

  @property
  def id(self):
    return self._id

  @id.setter
  def id(self, val):
    if (pyauto_base.misc.isEmptyString(val)):
      msg = "CANNOT assign empty id"
      logger.error(msg)
      raise RuntimeError(msg)
    self._id = val

  def updateMetadata(self, dictJson):
    self.id = str(dictJson["id"])
    self.name = dictJson["name"]
    self.description = dictJson["description"]
    self.public = dictJson["public_flag"]
    for ak in dictJson["api_keys"]:
      if (ak["write_flag"]):
        self.api_key_wr = ak["api_key"]
      else:
        self.api_key_rd = ak["api_key"]

  def updateFields(self, dictJson):
    self.fields = []
    for k in sorted(dictJson.keys()):
      if (k.startswith("field") and (dictJson[k] is not None)):
        self.fields.append(dictJson[k])

class ThingSpeak(libWeb.WebService):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(ThingSpeak, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)
    self._headers = {"THINGSPEAKAPIKEY": self._auth["pass"]}
    self.channels = {}

  def getChannelList(self):
    url = "{}/channels.json".format(self._endpoint["url"])
    res = requests.get(url, headers=self._headers)
    libWeb.chkRequestStatus(res)

    for val in res.json():
      if (val["id"] not in self.channels.keys()):
        ch = ThingSpeakChannel()
        ch.updateMetadata(val)
        self.channels[ch.id] = ch
      else:
        ch = self.channels[val["id"]]
        ch.updateMetadata(val)
    return res.json()

  def getChannelMetadata(self, chId):
    url = "{}/channels/{}.json".format(self._endpoint["url"], chId)
    res = requests.get(url, headers=self._headers)
    libWeb.chkRequestStatus(res)

    if (chId not in self.channels.keys()):
      ch = ThingSpeakChannel()
      ch.updateMetadata(res)
      self.channels[ch.id] = ch
    else:
      ch = self.channels[chId]
      ch.updateMetadata(res.json())
    return res.json()

  def showChannels(self):
    for ch in self.channels.values():
      logger.info(ch)

  def getChannelFeed(self, chId, nRes=10):
    if (chId not in self.channels.keys()):
      msg = "NO channel id: {}".format(chId)
      logger.error(msg)
      raise RuntimeError(msg)

    ch = self.channels[chId]
    url = "{}/channels/{}/feeds.json".format(self._endpoint["url"], ch.id)
    params = {"results": str(nRes)}

    if (not ch.public):
      if (ch.api_key_rd is None):
        msg = "channel id: {} HAS NO API key".format(ch.id)
        logger.error(msg)
        raise RuntimeError(msg)

    res = requests.get(url, params=params, headers=self._headers)
    libWeb.chkRequestStatus(res)

    ch.updateFields(res["channel"])
    return res.json()["feeds"]

  def updateChannel(self, chId):
    if (chId not in self.channels):
      msg = "NO channel id: {}".format(chId)
      logger.error(msg)
      raise RuntimeError(msg)

    ch = self.channels[chId]
    url = "{}/channels/{}.json".format(self._endpoint["url"], ch.id)
    params = {
        "name": ch.name,
        "description": ch.description,
        "public_flag": str(ch.public).lower()
    }
    for i in range(0, len(ch.fields)):
      params["field{}".format(i + 1)] = ch.fields[i]

    res = requests.put(url, params=params, headers=self._headers)
    libWeb.chkRequestStatus(res)

    ch.updateMetadata(res.json())

  def updateChannelFeed(self, chId, lstData):
    if (chId not in self.channels):
      msg = "NO channel id: {}".format(chId)
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(lstData) > 8):
      msg = "MAX data/channel is 8"
      logger.error(msg)
      raise RuntimeError(msg)

    ch = self.channels[chId]
    url = "{}/update.json".format(self._endpoint["url"])
    params = {}

    if (ch.api_key_wr is None):
      msg = "channel id: {} HAS NO API key".format(ch.id)
      logger.error(msg)
      raise RuntimeError(msg)

    if ((len(ch.fields) == 0) or (len(ch.fields) > len(lstData))):
      for i in range(0, len(lstData)):
        params["field{}".format(i + 1)] = lstData[i]
    else:
      for i in range(0, len(ch.fields)):
        params["field{}".format(i + 1)] = lstData[i]

    res = requests.post(url, params=params, headers=self._headers)
    libWeb.chkRequestStatus(res)

    ch.updateFields(res.json())

  def clearChannel(self, chId):
    if (chId not in self.channels.keys()):
      msg = "NO channel id: {}".format(chId)
      logger.error(msg)
      raise RuntimeError(msg)

    ch = self.channels[chId]
    url = "{}/channels/{}/feeds.json".format(self._endpoint["url"], ch.id)
    requests.delete(url, headers=self._headers)

class AdafruitFeed(object):

  def __init__(self):
    self._id = None
    self.key = None
    self.name = None
    self.description = None
    self.visibility = None

  def __str__(self):
    return "{}: {} (id: {}), visibility: {}".format(self.__class__.__name__, self.name,
                                                    self._id, self.visibility)

  @property
  def id(self):
    return self._id

  @id.setter
  def id(self, val):
    if (pyauto_base.misc.isEmptyString(val)):
      msg = "CANNOT assign empty id"
      logger.error(msg)
      raise RuntimeError(msg)
    self._id = val

  def updateMetadata(self, dictJson):
    self.id = str(dictJson["id"])
    self.key = dictJson["key"]
    self.name = dictJson["name"]
    self.description = dictJson["description"]
    self.visibility = dictJson["visibility"]

class Adafruit(libWeb.WebService):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(Adafruit, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)
    self._headers = {"X-Aio-Key": self._auth["pass"]}
    self.feeds = {}

  def getUser(self):
    url = "{}/user".format(self._endpoint["url"])
    res = requests.get(url, headers=self._headers)
    libWeb.chkRequestStatus(res)
    return res.json()

  def getActivities(self):
    url = "{}/{}/activities".format(self._endpoint["url"], self._auth["user"])
    res = requests.get(url, headers=self._headers)
    libWeb.chkRequestStatus(res)
    return res.json()

  def showActivities(self):
    res = self.getActivities()

    for val in res:
      info = "???"
      if ("data" in val.keys()):
        if (isinstance(val["data"], dict)):
          if ("name" in val["data"].keys()):
            info = val["data"]["name"]
          elif ("qr_code" in val["data"].keys()):
            info = "QR CODE"
        elif (isinstance(val["data"], str)):
          tmp = json.loads(val["data"])
          if ("name" in tmp.keys()):
            info = tmp["name"]
          elif ("qr_code" in tmp.keys()):
            info = "QR CODE"
        else:
          info = "!!! {} !!!".format(type(val["data"]))
      logger.info("activity {} {}: {}".format(val["action"], val["model"], info))

  def getFeeds(self):
    url = "{}/{}/feeds".format(self._endpoint["url"], self._auth["user"])
    res = requests.get(url, headers=self._headers)
    libWeb.chkRequestStatus(res)

    for val in res.json():
      if (val["id"] not in self.feeds.keys()):
        fd = AdafruitFeed()
        fd.updateMetadata(val)
        self.feeds[fd.id] = fd
      else:
        fd = self.feeds[val["id"]]
        fd.updateMetadata(val)
    return res.json()

  def showFeeds(self):
    for fd in self.feeds.values():
      logger.info(fd)

  def getFeedData(self, fdId, nRes=10):
    if (fdId not in self.feeds.keys()):
      msg = "NO feed id: {}".format(fdId)
      logger.error(msg)
      raise RuntimeError(msg)

    fd = self.feeds[fdId]
    url = "{}/{}/feeds/{}/data".format(self._endpoint["url"], self._auth["user"], fd.key)
    params = {"limit": str(nRes)}
    res = requests.get(url, params=params, headers=self._headers)
    libWeb.chkRequestStatus(res)
    return res.json()

  def updateFeed(self, fdId):
    if (fdId not in self.feeds):
      msg = "NO feed id: {}".format(fdId)
      logger.error(msg)
      raise RuntimeError(msg)

    fd = self.feeds[fdId]
    url = "{}/{}/feeds/{}".format(self._endpoint["url"], self._auth["user"], fd.key)
    data = {"name": fd.name, "description": fd.description}

    res = requests.patch(url, headers=self._headers, data=data)
    libWeb.chkRequestStatus(res)
    fd.updateMetadata(res.json())

  def addFeedData(self, fdId, val):
    if (fdId not in self.feeds):
      msg = "NO feed id: {}".format(fdId)
      logger.error(msg)
      raise RuntimeError(msg)

    fd = self.feeds[fdId]
    url = "{}/{}/feeds/{}/data".format(self._endpoint["url"], self._auth["user"], fd.key)
    data = {"value": str(val)}

    res = requests.post(url, headers=self._headers, data=data)
    libWeb.chkRequestStatus(res)

  def delFeedData(self, fdId, dataId):
    if (fdId not in self.feeds.keys()):
      msg = "NO feed id: {}".format(fdId)
      logger.error(msg)
      raise RuntimeError(msg)

    fd = self.feeds[fdId]
    url = "{}/{}/feeds/{}/data/{}".format(self._endpoint["url"], self._auth["user"], fd.key,
                                          dataId)

    res = requests.delete(url, headers=self._headers)
    libWeb.chkRequestStatus(res)

  def connect(self, **kwargs):
    self.getFeeds()

  def send(self, **kwargs):
    setKeys = {"feedId", "value", "unit", "desc", "timestamp"}
    setDiff = setKeys - set(kwargs.keys())
    if (len(setDiff) > 0):
      msg = "MISSING send parameters: {}".format(setKeys)
      logger.error(msg)
      raise RuntimeError(msg)

    self.addFeedData(fdId=kwargs["feedId"], val=kwargs["value"])

class AdafruitMQTT(MQTTDataService):

  def __init__(self, cfgPath=None):
    super(AdafruitMQTT, self).__init__(cfgPath=cfgPath)

  def send(self, **kwargs):
    setKeys = {"pub", "value", "unit", "desc", "timestamp"}
    setDiff = setKeys - set(kwargs.keys())
    if (len(setDiff) > 0):
      msg = "MISSING send parameters: {}".format(setKeys)
      logger.error(msg)
      raise RuntimeError(msg)

    self._conn.publish(kwargs["pub"], str(kwargs["value"]))
