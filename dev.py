#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import requests

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_base.config
import pyauto_comm.web as libWeb

def enableCertVerification():
  libWeb.enableCertVerification()

def disableCertVerification():
  libWeb.disableCertVerification()

class Github(libWeb.WebService):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(Github, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)
    self._headers = {
        "Accept": "application/vnd.github+json",
        "Authorization": "Bearer {}".format(self._auth["pass"])
    }

  def getUser(self, user=None):
    if (pyauto_base.misc.isEmptyString(user)):
      url = "{}/user".format(self._endpoint["url"])
      res = requests.get(url, headers=self._headers)
    else:
      url = "{}/users/{}".format(self._endpoint["url"], user)
      res = requests.get(url)
    libWeb.chkRequestStatus(res)
    return res.json()

  def getOrg(self, org):
    if (pyauto_base.misc.isEmptyString(org)):
      url = "{}/user/orgs".format(self._endpoint["url"])
      res = requests.get(url, headers=self._headers)
    else:
      url = "{}/orgs/{}".format(self._endpoint["url"], org)
      res = requests.get(url)
    libWeb.chkRequestStatus(res)
    return res.json()

  def getOrgStats(self, org):
    if (pyauto_base.misc.isEmptyString(org)):
      logger.warning("CANNOT get EMPTY organization")
      return

    dictRepo = {}
    url = "{}/orgs/{}/repos".format(self._endpoint["url"], org)
    res = requests.get(url, headers=self._headers)
    libWeb.chkRequestStatus(res)
    for repo in res.json():
      #for k, v in repo.items():
      #  logger.info("  {}: {}".format(k, v))
      repoInfo = {
          "language": repo["language"],
          "issues": repo["open_issues_count"],
          #"forks": repo["forks_count"],
          "pulls_url": repo["pulls_url"].replace("{/number}", ""),
          "branches_url": repo["branches_url"].replace("{/branch}", ""),
          #"releases_url": repo["releases_url"].replace("{/id", ""),
      }
      dictRepo[repo["name"]] = repoInfo

    for k, v in dictRepo.items():
      #logger.info("pull requests for {}".format(k))
      pyauto_base.misc.waitWithPrint(self.retryTimeout)
      res = requests.get(v["pulls_url"], headers=self._headers)
      libWeb.chkRequestStatus(res)
      v["pull_reqs"] = len(res.json())
      del v["pulls_url"]

    for k, v in dictRepo.items():
      #logger.info("pull requests for {}".format(k))
      pyauto_base.misc.waitWithPrint(self.retryTimeout)
      res = requests.get(v["branches_url"], headers=self._headers)
      libWeb.chkRequestStatus(res)
      v["branches"] = len(res.json())
      del v["branches_url"]

    logger.info("found {} repo(s)".format(len(dictRepo)))
    for k, v in dictRepo.items():
      logger.info("{}: {}".format(k, v))

    return dictRepo
