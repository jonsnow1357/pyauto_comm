#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library for web related protocols (email, HTTP, HTTPS, ...)"""

#import site #http://docs.python.org/library/site.html
import sys
import os
import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import urllib.parse
import requests
import requests_oauthlib
import xml.etree.ElementTree as ET

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config

_bCertVerify = True
_timeout = 4.0

def enableCertVerification():
  global _bCertVerify
  _bCertVerify = True

def disableCertVerification():
  global _bCertVerify
  _bCertVerify = False

class WebServiceException(Exception):
  pass

class WebTimeoutException(WebServiceException):
  pass

def _request_reply_OK(reqReply, reqName):
  if (reqReply.status_code != 200):
    logger.error("{} status: {}".format(reqName, reqReply.status_code))
    logger.error(reqReply.text)
    raise WebServiceException(reqReply)

def chkRequestStatus(reqReply):
  if (reqReply.status_code != 200):
    logger.error("status: {}".format(reqReply.status_code))
    logger.error(reqReply.text)
    raise WebServiceException(reqReply.text)

def fail_WebService(webs, msg):
  tmp = "WebService {}: {}".format(webs.__class__.__name__, msg)
  logger.error(tmp)
  raise WebServiceException(tmp)

authTypeTLS = "tls"
authTypeBasic = "basic"
authTypeHeader = "header"
authTypeOAuth1 = "oauth1"
authTypeOAuth2 = "oauth2"
authTypeCustom = "custom"
authTypes = (None, authTypeTLS, authTypeBasic, authTypeHeader, authTypeOAuth1,
             authTypeOAuth2, authTypeCustom)

class _ServiceBase(object):

  def __init__(self, cfgPath=None, cfgSection=None):
    self._endpoint = {"url": ""}
    self._auth = {"type": None, "user": None, "pass": None}
    self._oauth_token = {}
    self._params = {}

    if (cfgPath is None):
      lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
      cfgPath = os.path.join(lclDir, "cfg", (self.__class__.__name__ + ".ini"))
    self._cfgPath = os.path.abspath(cfgPath)
    if (cfgSection is None):
      cfgSection = "default"
    self._cfgSection = cfgSection

    if (not pyauto_base.fs.chkPath_File(self._cfgPath, bDie=False)):
      fail_WebService(self, "NO config")
    if (self._cfgPath.endswith(".ini") or self._cfgPath.endswith(".cfg")):
      self._parseCfg()
    elif (cfgPath.endswith(".xml")):
      self._parseXml()
    else:
      fail_WebService(self, "INCORRECT config")

  def __str__(self):
    return "{}: {} (auth {})".format(self.__class__.__name__, self._endpoint["url"],
                                     self._auth["type"])

  def _parseCfg(self):
    logger.info("read config file: {}".format(self._cfgPath))
    cfgObj = pyauto_base.config.DictConfig()
    dictCfg = cfgObj.readCfg(self._cfgPath)[self._cfgSection]
    for k, v in dictCfg.items():
      if (k in ("api_url", "smtp_host")):
        self._endpoint["url"] = v
      elif (k.startswith("api_")):
        self._endpoint[k[4:]] = v
      elif (k == "auth_type"):
        self._auth["type"] = v
      elif (k in ("auth_user", "auth_client", "auth_client_id")):
        self._auth["user"] = v
      elif (k in ("auth_password", "auth_token", "auth_client_secret")):
        self._auth["pass"] = v
      elif (k == "oauth_token"):
        self._oauth_token = json.loads(v)
      else:
        self._params[k] = v

  def _saveCfg(self):
    logger.info("save config file: {}".format(self._cfgPath))
    cfgObj = pyauto_base.config.DictConfig()
    dictCfg = cfgObj.readCfg(self._cfgPath)[self._cfgSection]
    for k, v in dictCfg.items():
      if (k in ("api_url", "smtp_host")):
        dictCfg[k] = self._endpoint["url"]
      elif (k.startswith("api_")):
        pass  # updated below
      elif (k == "auth_type"):
        dictCfg[k] = self._auth["type"]
      elif (k in ("auth_user", "auth_client", "auth_client_id")):
        dictCfg[k] = self._auth["user"]
      elif (k in ("auth_password", "auth_token", "auth_client_secret")):
        dictCfg[k] = self._auth["pass"]
      elif (k == "oauth_token"):
        pass  # updated below
      else:
        dictCfg[k] = self._params[k]

    for k, v in self._endpoint.items():
      if (k != "api_url"):
        dictCfg["api_" + k] = self._endpoint[k]
    if (len(self._oauth_token) > 0):
      dictCfg["oauth_token"] = json.dumps(self._oauth_token)
    cfgObj.writeCfg(self._cfgPath, default=dictCfg)

  def _parseXml(self):
    logger.info("read config file: {}".format(self._cfgPath))
    domObj = ET.parse(self._cfgPath)
    for elWeb in domObj.getroot().findall("web"):
      if (elWeb.attrib["id"] != self.__class__.__name__):
        continue
      for el in elWeb.iter():
        if (el.tag == "web"):
          continue
        elif (el.tag in ("api_url", "smtp_host")):
          self._endpoint["url"] = el.text
        elif (el.tag.startswith("api_")):
          self._endpoint[el.tag[4:]] = el.text
        elif (el.tag == "auth_type"):
          self._auth["type"] = el.text
        elif (el.tag in ("auth_user", "auth_client", "auth_client_id")):
          self._auth["user"] = el.text
        elif (el.tag in ("auth_password", "auth_token", "auth_client_secret")):
          self._auth["pass"] = el.text
        elif (el.tag == "oauth_token"):
          self._oauth_token = json.loads(el.text)
        else:
          self._params[el.tag] = el.text

  def _check(self):
    if ("url" not in self._endpoint.keys()):
      fail_WebService(self, "INCORRECT config: no host/URL")
    if (pyauto_base.misc.isEmptyString(self._endpoint["url"])):
      fail_WebService(self, "INCORRECT config: empty host/URL")

    if ("type" not in self._auth.keys()):
      fail_WebService(self, "INCORRECT config: no auth")
    if (self._auth["type"] not in authTypes):
      fail_WebService(
          self, "INCORRECT config: UNEXPECTED auth_type {}".format(self._auth["type"]))

class WebService(_ServiceBase):
  """
  class for a service with API and some kind of authentication
  """

  def __init__(self, cfgPath=None, cfgSection=None):
    super(WebService, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)

    self.retryLimit = 1
    self.retryTimeout = 10  # seconds
    self.minMesasgeSize = 10
    self.maxMesasgeSize = 140

    self._check()

    self._rsrcObj = None
    if (self._auth["type"] == "oauth1"):
      self._initOAuth1()
    elif (self._auth["type"] == "oauth2"):
      self._initOAuth2()

  def _initOAuth1(self):
    if (len(self._oauth_token) == 0):
      self._rsrcObj = requests_oauthlib.OAuth1Session(
          self._auth["user"],
          client_secret=self._auth["pass"],
      )
      fetch_response = self._rsrcObj.fetch_request_token(
          self._endpoint["oauth1_request_token"])
      resource_owner_key = fetch_response.get('oauth_token')
      resource_owner_secret = fetch_response.get('oauth_token_secret')
      authorization_url = self._rsrcObj.authorization_url(self._endpoint["oauth1"])
      logger.info("go to this URL and approve access:\n{}".format(authorization_url))
      redirect_response = input("Paste the full redirect URL here: ")

      oauth_response = self._rsrcObj.parse_authorization_response(redirect_response)
      verifier = oauth_response.get('oauth_verifier')
      self._rsrcObj = requests_oauthlib.OAuth1Session(
          self._auth["user"],
          client_secret=self._auth["pass"],
          resource_owner_key=resource_owner_key,
          resource_owner_secret=resource_owner_secret,
          verifier=verifier)
      token = self._rsrcObj.fetch_access_token(self._endpoint["oauth1_access_token"])
      self._updateOAuthToken(token)
    else:
      self._rsrcObj = requests_oauthlib.OAuth1Session(
          self._auth["user"],
          client_secret=self._auth["pass"],
          resource_owner_key=self._oauth_token["owner_key"],
          resource_owner_secret=self._oauth_token["owner_secret"])
    logger.info("OAuth1 done")

  def _initOAuth2(self):
    if (len(self._oauth_token) == 0):
      self._rsrcObj = requests_oauthlib.OAuth2Session(
          client_id=self._auth["user"], redirect_uri=self._endpoint["oauth2_redirect"])
      authorization_url, state = self._rsrcObj.authorization_url(
          url=self._endpoint["oauth2"])
      logger.info("go to this URL and approve access:\n{}".format(authorization_url))
      redirect_response = input("Enter the full callback URL from browser: ")

      parsed_url = urllib.parse.urlparse(redirect_response)
      parsed_qry = urllib.parse.parse_qs(parsed_url.query)
      #print("DBG", parsed_qry["code"][0])
      token = self._rsrcObj.fetch_token(token_url=self._endpoint["oauth2_fetch"],
                                        code=parsed_qry["code"][0],
                                        include_client_id=True,
                                        client_secret=self._auth["pass"])
      self._updateOAuthToken(token)
    else:
      if ("oauth2_refresh" in self._endpoint.keys()):
        self._rsrcObj = requests_oauthlib.OAuth2Session(
            client_id=self._auth["user"],
            token=self._oauth_token,
            auto_refresh_url=self._endpoint["oauth2_refresh"],
            auto_refresh_kwargs={
                "client_id": self._auth["user"],
                "client_secret": self._auth["pass"]
            },
            token_updater=self._updateOAuthToken,
        )
      else:
        self._rsrcObj = requests_oauthlib.OAuth2Session(client_id=self._auth["user"],
                                                        token=self._oauth_token)
    logger.info("OAuth2 done")

  def _updateOAuthToken(self, token):
    self._oauth_token = {}
    self._oauth_token.update(token)
    self._saveCfg()

  @property
  def apiBase(self):
    return self._endpoint["url"]

  @property
  def user(self):
    return self._auth["user"]

  # def _oauth1(self):
  #   import requests_oauthlib
  #
  #   logger.info("OAuth1")
  #
  #   # get request token
  #   if (self._oauth["url_request_token"] is None):
  #     msg = "INCOMPLETE OAuth info: no url_request_token"
  #     logger.error(msg)
  #     raise RuntimeError(msg)
  #   self._apiAuth = requests_oauthlib.OAuth1(self._oauth["client"],
  #                                            client_secret=self._oauth["client_secret"])
  #   res = POST(self._oauth["url_request_token"], auth=self._apiAuth)
  #   #logger.info(res)
  #   if (sys.version_info[0] == 2):
  #     if (isinstance(res, unicode)):
  #       res = str(res)
  #   if (isinstance(res, six.string_types)):
  #     tmp = six.moves.urllib_parse.parse_qs(res)
  #     req_token = tmp["oauth_token"][0]
  #     req_token_secret = tmp["oauth_token_secret"][0]
  #   else:
  #     raise NotImplementedError  # TODO:  json reply
  #
  #   # get authorization (http://requests-oauthlib.readthedocs.io/en/latest/oauth1_workflow.html)
  #   if (self._oauth["url_authorize"] is None):
  #     verifier = None
  #   else:
  #     logger.info("Authorization NEEDED: {}?oauth_token={}".format(
  #         self._oauth["url_authorize"], req_token))
  #     verifier = six.moves.input("Please input the verifier:")
  #     raise NotImplementedError  # TODO:
  #
  #   # get access token
  #   if (self._oauth["url_exchange_token"] is None):
  #     msg = "INCOMPLETE OAuth info: no url_exchange_token"
  #     logger.error(msg)
  #     raise RuntimeError(msg)
  #   self._apiAuth = requests_oauthlib.OAuth1(self._oauth["client"],
  #                                            client_secret=self._oauth["client_secret"],
  #                                            resource_owner_key=req_token,
  #                                            resource_owner_secret=req_token_secret,
  #                                            verifier=verifier)
  #   res = POST(self._oauth["url_exchange_token"], auth=self._apiAuth)
  #   #logger.info(res)
  #   if (sys.version_info[0] == 2):
  #     if (isinstance(res, unicode)):
  #       res = str(res)
  #   if (isinstance(res, six.string_types)):
  #     tmp = six.moves.urllib_parse.parse_qs(res)
  #     self._oauth["token"] = tmp["oauth_token"][0]
  #     self._oauth["token_secret"] = tmp["oauth_token_secret"][0]
  #   else:
  #     raise NotImplementedError  # TODO:  json reply
  #
  #   self._oauth["ver"] = "1"
  #   self._apiAuth = requests_oauthlib.OAuth1(
  #       self._oauth["client"],
  #       client_secret=self._oauth["client_secret"],
  #       resource_owner_key=self._oauth["token"],
  #       resource_owner_secret=self._oauth["token_secret"])
  #
  # def _get_oauth1(self):
  #   import requests_oauthlib
  #
  #   if (self._oauth["token"] is None):
  #     msg = "MISSING OAuth1 token"
  #     logger.error(msg)
  #     raise RuntimeError(msg)
  #
  #   if (self._apiAuth is None):
  #     self._apiAuth = requests_oauthlib.OAuth1(
  #         self._oauth["client"],
  #         client_secret=self._oauth["client_secret"],
  #         resource_owner_key=self._oauth["token"],
  #         resource_owner_secret=self._oauth["token_secret"])
  #   return self._apiAuth

  # def _oauth2_code_grant(self):
  #   import requests_oauthlib
  #
  #   logger.info("OAuth2 Code Grant")
  #   if (self._oauth["url_authorize"] is None):
  #     msg = "INCOMPLETE OAuth info: no url_authorize"
  #     logger.error(msg)
  #     raise RuntimeError(msg)
  #
  #   #if(self._oauth["url_authorize"].startswith("http://")):
  #   #  os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
  #   sess = requests_oauthlib.OAuth2Session(self._oauth["client"])
  #   authorization_url, state = sess.authorization_url(self._oauth["url_authorize"])
  #   logger.info("Authorization NEEDED: {}".format(authorization_url))
  #   verifier = six.moves.input("Please input the verifier:")
  #   sess.fetch_token(self._oauth["url_exchange_token"],
  #                    client_secret=self._oauth["client_secret"],
  #                    authorization_response=verifier)
  #   self._oauth["token"] = sess.token
  #
  #   self._oauth["ver"] = "2"
  #   self._apiAuth = requests_oauthlib.OAuth2(self._oauth["client"],
  #                                            token=self._oauth["token"])
  #
  # def _get_oauth2(self):
  #   import requests_oauthlib
  #
  #   if (self._oauth["token"] is None):
  #     msg = "MISSING OAuth2 token"
  #     logger.error(msg)
  #     raise RuntimeError(msg)
  #
  #   if (self._apiAuth is None):
  #     self._apiAuth = requests_oauthlib.OAuth2(self._oauth["client"],
  #                                              token=self._oauth["token"])
  #   return self._apiAuth

  # def _get_auth(self):
  #   raise NotImplementedError
  #
  # def getAuth(self):
  #   import requests.auth
  #   import requests_oauthlib
  #
  #   if (self._auth["type"] is None):
  #     return
  #
  #   if (self._auth["type"] == "basic"):
  #     if (self._auth["pass"] is not None):
  #       return requests.auth.HTTPBasicAuth(self._auth["user"], self._auth["pass"])
  #     if (self._auth["pass"] is not None):
  #       return requests.auth.HTTPBasicAuth(self._auth["user"], self._auth["pass"])
  #
  #   if (self._auth["type"] == "oauth1"):
  #     return requests_oauthlib.OAuth1(self._oauth["client"],
  #                                     client_secret=self._oauth["client_secret"],
  #                                     resource_owner_key=self._oauth["token"],
  #                                     resource_owner_secret=self._oauth["token_secret"])
  #
  #   if (self._auth["type"] == "oauth2"):
  #     return requests_oauthlib.OAuth2(None, token=json.loads(self._oauth["token"]))
  #   #oauthInfo = self._config.oauthInfo
  #   #if (self._oauth["ver"] == "1"):
  #   #  return self._get_oauth1()
  #   #elif (self._oauth["ver"] == "2"):
  #   #  return self._get_oauth2()
  #   #else:
  #   #  return self._get_auth()

class Email(_ServiceBase):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(Email, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)

    self.minMesasgeSize = 10
    self.maxMesasgeSize = 16384

    self._check()

  @property
  def addrFrom(self):
    return self._params["from"]

  @property
  def addrTo(self):
    return self._params["to"]

  def _createMessage(self, body, subj=None, attach=None):
    import getpass
    import email.mime.base
    import email.mime.text
    import email.mime.multipart
    import email.utils
    import email.encoders
    import mimetypes

    eml = email.mime.multipart.MIMEMultipart("mixed")

    if (pyauto_base.misc.isEmptyString(subj)):
      subj = "{} <{}@{}>".format(os.path.basename(__file__), getpass.getuser(),
                                 platform.uname()[1])
    else:
      subj = "{} <{}@{}> {}".format(os.path.basename(__file__), getpass.getuser(),
                                    platform.uname()[1], subj)
    eml["Date"] = email.utils.formatdate(localtime=True)
    eml["Subject"] = subj
    eml["X-Mailer"] = "{}.{}".format(self.__module__, self.__class__.__name__)

    eml.attach(email.mime.text.MIMEText(body))
    if ((attach is not None) and (pyauto_base.fs.chkPath_File(attach, bDie=False))):
      mimetype, encoding = mimetypes.guess_type(attach)
      if (mimetype is None):
        mimetype = "application/octet-stream"
      attachment = email.mime.base.MIMEBase(*mimetype.split("/"))
      with open(attach, "r") as fIn:
        attachment.set_payload(fIn.read())
      email.encoders.encode_base64(attachment)
      attachment.add_header("Content-Disposition",
                            "attachment",
                            filename=os.path.basename(attach))
      eml.attach(attachment)
    return eml

  def sendMessage(self, msg, bSend=True, **kwargs):
    import smtplib
    import ssl

    if (pyauto_base.misc.isEmptyString(msg)):
      logger.warning("WILL NOT send empty message")
      return
    if (len(msg) < self.minMesasgeSize):
      logger.warning("message to small:\n{}".format(msg))
      return
    if (len(msg) > self.maxMesasgeSize):
      logger.warning("message to big:\n{}".format(msg))
      return

    if ("to" not in kwargs.keys()):
      msg = "NO address to send email"
      logger.error(msg)
      raise WebServiceException(msg)
    if (not isinstance(kwargs["to"], list)):
      if (pyauto_base.misc.isEmptyString(kwargs["to"])):
        msg = "NO address to send email"
        logger.error(msg)
        raise WebServiceException(msg)
      logger.warning("TO is not a list")
      msg_to = [str(kwargs["to"])]
    else:
      if (len(kwargs["to"]) == 0):
        msg = "NO address to send email"
        logger.error(msg)
        raise WebServiceException(msg)
      msg_to = kwargs["to"]

    if ("subject" in kwargs.keys()):
      email_subj = kwargs["subject"]
    else:
      email_subj = None
    if ("attach" in kwargs.keys()):
      email_attach = kwargs["attach"]
    else:
      email_attach = None
    email = self._createMessage(msg, subj=email_subj, attach=email_attach)
    email["From"] = self._auth["user"]
    email["To"] = ",".join(msg_to)

    logger.info("send email to: {}".format(msg_to))
    if (not bSend):
      return

    conn = smtplib.SMTP(self._endpoint["url"])
    try:
      conn.ehlo()
      if (self._auth["type"] == authTypeTLS):
        ssl_context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLS_CLIENT)
        #ssl_context.options |= ssl.OP_NO_SSLv2
        #ssl_context.options |= ssl.OP_NO_SSLv3
        #ssl_context.set_ciphers("ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:"
        #                        "ECDH+AES128:DH+AES:ECDH+HIGH:DH+HIGH:ECDH+3DES:DH+3DES:"
        #                        "RSA+AESGCM:RSA+AES:RSA+HIGH:RSA+3DES:!aNULL:!eNULL:!MD5")
        conn.starttls()
        conn.ehlo()
      if (self._auth["type"] in (authTypeBasic, authTypeTLS)):
        conn.login(user=self._auth["user"], password=self._auth["pass"])
      conn.sendmail(self._auth["user"], msg_to, email.as_string())
    except (smtplib.SMTPHeloError, smtplib.SMTPAuthenticationError,
            smtplib.SMTPNotSupportedError, smtplib.SMTPDataError, smtplib.SMTPException,
            ssl.SSLError) as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise WebServiceException(msg)
    finally:
      conn.quit()
