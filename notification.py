#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import requests

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config
import pyauto_comm.web as libWeb

def enableCertVerification():
  libWeb.enableCertVerification()

def disableCertVerification():
  libWeb.disableCertVerification()

class Mastodon(libWeb.WebService):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(Mastodon, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)
    self._headers = {"Authorization": "Bearer {}".format(self._auth["pass"])}
    self.userInfo = {}
    self.getUserInfo()

  def showUserInfo(self):
    logger.info("{} (id {}): {}".format(self.userInfo["username"], self.userInfo["id"],
                                        self.userInfo["url"]))

  def getUserInfo(self):
    url = "{}/accounts/verify_credentials".format(self._endpoint["url"])
    res = requests.get(url, headers=self._headers)
    libWeb.chkRequestStatus(res)
    res = res.json()

    self.userInfo["id"] = str(res["id"])
    self.userInfo["username"] = res["username"]
    self.userInfo["url"] = res["url"]

  def getTimeline(self):
    url = "{}/timelines/home".format(self._endpoint["url"])
    #url = "{}/timelines/public".format(self.API["url"])
    res = requests.get(url, headers=self._headers)
    libWeb.chkRequestStatus(res)

    lstId = []
    for tmp in res.json():
      post_id = tmp["id"]
      logger.info("post by {} @ {}".format(tmp["account"]["username"], tmp["created_at"]))
      lstId.append(post_id)
    logger.info("{:d} post(s)".format(len(lstId)))
    return lstId

class Twitter(libWeb.WebService):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(Twitter, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)
    self.userInfo = {}
    self.getUserInfo()

  def showUserInfo(self):
    logger.info("{} (id {})".format(self.userInfo["screen_name"], self.userInfo["id"]))

  def getUserInfo(self):
    url = "{}/account/verify_credentials.json".format(self._endpoint["url"])
    res = self._rsrcObj.get(url)
    libWeb.chkRequestStatus(res)
    res = res.json()

    self.userInfo["id"] = str(res["id"])
    self.userInfo["screen_name"] = res["screen_name"]

  def getTimeline(self):
    url = "{}/statuses/home_timeline.json".format(self._endpoint["url"])
    #url = "{}/statuses/user_timeline.json".format(self.API["url"])
    res = self._rsrcObj.get(url)
    libWeb.chkRequestStatus(res)

    lstId = []
    for tmp in res.json():
      post_id = tmp["id"]
      logger.info("post by {} @ {}".format(tmp["user"]["screen_name"], tmp["created_at"]))
      lstId.append(post_id)
    logger.info("{:d} post(s)".format(len(lstId)))
    return lstId

  def sendMessage(self, msg, bSend=True, **kwargs):
    if (pyauto_base.misc.isEmptyString(msg)):
      logger.warning("WILL NOT send empty message")
      return
    if (len(msg) < self.minMesasgeSize):
      logger.warning("message to small:\n{}".format(msg))
      return
    if (len(msg) > self.maxMesasgeSize):
      logger.warning("message to big:\n{}".format(msg))
      return
    if (not bSend):
      logger.info("send: {}".format(msg))
      return

    url = "{}/statuses/update.json".format(self._endpoint["url"])
    params = {"status": msg}
    res = self._rsrcObj.post(url, params=params)
    libWeb.chkRequestStatus(res)

  def delMessage(self, msgId, bSend=True):
    if (not bSend):
      logger.info("delete: {}".format(msgId))
      return

    url = "{}/statuses/destroy/{}.json".format(self._endpoint["url"], msgId)
    params = {"id": msgId}
    res = self._rsrcObj.post(url, params=params)
    libWeb.chkRequestStatus(res)

class Pushover(libWeb.WebService):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(Pushover, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)
    self.checkUser()

  def checkUser(self):
    url = "{}/users/validate.json".format(self._endpoint["url"])
    data = {"user": self._auth["user"], "token": self._auth["pass"]}
    res = requests.post(url, data=data)
    libWeb.chkRequestStatus(res)

  def sendMessage(self, msg, bSend=True, **kwargs):
    if (pyauto_base.misc.isEmptyString(msg)):
      logger.warning("WILL NOT send empty message")
      return
    if (len(msg) < self.minMesasgeSize):
      logger.warning("message to small:\n{}".format(msg))
      return

    device = None
    if ("device" in kwargs.keys()):
      device = kwargs["device"]
    if (pyauto_base.misc.isEmptyString(device)):
      device = self._params["device"]
    if (pyauto_base.misc.isEmptyString(device)):
      logger.warning("a device must be specified")
      return

    title = None
    if ("title" in kwargs.keys()):
      title = kwargs["title"]

    if (not bSend):
      logger.info("[test]@{} '{}'".format(device, msg))
      return

    url = "{}/messages.json".format(self._endpoint["url"])
    data = {
        "user": self._auth["user"],
        "token": self._auth["pass"],
        "device": device,
        "message": msg
    }
    if (not pyauto_base.misc.isEmptyString(title)):
      data["title"] = title

    #logger.info("@{} '{}'".format(device, msg))
    res = requests.post(url, data=data)
    libWeb.chkRequestStatus(res)

class Slack(libWeb.WebService):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(Slack, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)
    self.checkAPI()

  def checkAPI(self):
    url = "{}/api.test".format(self._endpoint["url"])
    res = self._rsrcObj.get(url)
    libWeb.chkRequestStatus(res)
    if (res.json()["ok"] is not True):
      msg = "API FAIL: {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)

    url = "{}/auth.test".format(self._endpoint["url"])
    res = self._rsrcObj.get(url)
    libWeb.chkRequestStatus(res)
    if (res.json()["ok"] is not True):
      msg = "Auth FAIL: {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)

  def sendMessage(self, msg, bSend=True, **kwargs):
    if (pyauto_base.misc.isEmptyString(msg)):
      logger.warning("WILL NOT send empty message")
      return
    if ("channel" not in kwargs.keys()):
      logger.warning("channel NOT SPECIFIED")
      return

    channel = "webhook_{}".format(kwargs["channel"])
    if (channel not in self._params.keys()):
      logger.warning("channel {} NOT CONFIGURED".format(channel))
      return

    if (not bSend):
      logger.info("[test](@{}) {}".format(channel, msg))
      return

    url = self._endpoint[channel]
    headers = {"Content-type": "application/json"}
    data = json.dumps({"text": msg})  # payload should be a JSON string
    res = self._rsrcObj.post(url, headers=headers, data=data)
    libWeb.chkRequestStatus(res)
    #logger.info(res.json())
