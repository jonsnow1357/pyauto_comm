#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_comm.web"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_comm.web

# class Test_HTTPMethods(unittest.TestCase):
#   cwd = ""
#   lclDir = ""
#
#   @classmethod
#   def setUpClass(cls):
#     cls.cwd = os.getcwd()
#     cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
#     os.chdir(cls.lclDir)
#     logger.info("CWD: {}".format(os.getcwd()))
#
#   @classmethod
#   def tearDownClass(cls):
#     os.chdir(cls.cwd)
#     logger.info("CWD: {}".format(os.getcwd()))
#
#   def setUp(self):
#     print("")
#     self.inFolder = os.path.join(self.lclDir, "files")
#     self.outFolder = os.path.join(self.lclDir, "data_out")
#     if (not os.path.isdir(self.outFolder)):
#       os.makedirs(self.outFolder)
#
#   def tearDown(self):
#     pass
#
#   #@unittest.skip("")
#   def test_base(self):
#     res = pyauto_comm.web.GET("http://httpbin.org/get",
#                               params={"key1": "value1"},
#                               data={"key2": "value2"})
#     # logger.info(res)
#     self.assertEqual(type(res), type({}))
#     self.assertGreater(len(res), 0)
#     self.assertEqual(res["args"], {"key1": "value1"})
#     self.assertGreater(len(res["headers"]), 1)
#
#     res = pyauto_comm.web.POST("http://httpbin.org/post",
#                                params={"key1": "value1"},
#                                data={"key2": "value2"})
#     #logger.info(res)
#     self.assertEqual(type(res), type({}))
#     self.assertGreater(len(res), 0)
#     self.assertEqual(res["form"], {"key2": "value2"})
#     self.assertGreater(len(res["headers"]), 1)
#
#     res = pyauto_comm.web.PUT("http://httpbin.org/put",
#                               params={"key1": "value1"},
#                               data={"key2": "value2"})
#     #logger.info(res)
#     self.assertEqual(type(res), type({}))
#     self.assertGreater(len(res), 0)
#     self.assertEqual(res["args"], {"key1": "value1"})
#     self.assertEqual(res["form"], {"key2": "value2"})
#     self.assertGreater(len(res["headers"]), 1)
#
#     res = pyauto_comm.web.PATCH("http://httpbin.org/patch",
#                                 params={"key1": "value1"},
#                                 data={"key2": "value2"})
#     #logger.info(res)
#     self.assertEqual(type(res), type({}))
#     self.assertGreater(len(res), 0)
#     self.assertEqual(res["args"], {"key1": "value1"})
#     self.assertEqual(res["form"], {"key2": "value2"})
#     self.assertGreater(len(res["headers"]), 1)
#
#     res = pyauto_comm.web.DELETE("http://httpbin.org/delete",
#                                  params={"key1": "value1"},
#                                  data={"key2": "value2"})
#     #logger.info(res)
#     self.assertEqual(type(res), type({}))
#     self.assertGreater(len(res), 0)
#     self.assertEqual(res["args"], {"key1": "value1"})
#     self.assertEqual(res["form"], {"key2": "value2"})
#     self.assertGreater(len(res["headers"]), 1)

class Test_WebService(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base1(self):
    serv = pyauto_comm.web.WebService(os.path.join(self.inFolder, "web_api.ini"))
    self.assertEqual(serv._endpoint["url"], "http://website/api")
    self.assertEqual(serv._auth["type"], "basic")
    self.assertEqual(serv._auth["user"], "user")
    self.assertEqual(serv._auth["pass"], "password")
    self.assertDictEqual(serv._params, {})

  #@unittest.skip("")
  def test_base2(self):
    serv = pyauto_comm.web.WebService(os.path.join(self.inFolder, "web_email.ini"))
    self.assertEqual(serv._endpoint["url"], "smtp.example.com")
    self.assertEqual(serv._auth["type"], None)
    self.assertEqual(serv._auth["user"], "user@example.com")
    self.assertEqual(serv._auth["pass"], "password")
    self.assertDictEqual(serv._params, {"from": "somebody@example.com"})

  #@unittest.skip("")
  def test_base3(self):
    serv = pyauto_comm.web.WebService(os.path.join(self.inFolder, "web_api.xml"))
    self.assertEqual(serv._endpoint["url"], "http://website/api")
    self.assertEqual(serv._auth["type"], "basic")
    self.assertEqual(serv._auth["user"], "user")
    self.assertEqual(serv._auth["pass"], "password")
    self.assertDictEqual(serv._params, {})

  #@unittest.skip("")
  def test_base4(self):
    serv = pyauto_comm.web.WebService(os.path.join(self.inFolder, "web_email.xml"))
    self.assertEqual(serv._endpoint["url"], "smtp.example.com")
    self.assertEqual(serv._auth["type"], None)
    self.assertEqual(serv._auth["user"], "user@example.com")
    self.assertEqual(serv._auth["pass"], "password")
    self.assertDictEqual(serv._params, {"from": "somebody@example.com"})

class Test_email(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base1(self):
    try:
      serv = pyauto_comm.web.Email()
    except pyauto_comm.web.WebServiceException:
      self.skipTest("")
      serv = None

    logger.info(serv)
    msg = '''
        Beautiful is better than ugly.
        Explicit is better than implicit.
        Simple is better than complex.
        Complex is better than complicated.
        Flat is better than nested.
        Sparse is better than dense.
        Readability counts.
        Special cases aren't special enough to break the rules.
        Although practicality beats purity.
        Errors should never pass silently.
        Unless explicitly silenced.
        In the face of ambiguity, refuse the temptation to guess.
        There should be one-- and preferably only one --obvious way to do it.
        Although that way may not be obvious at first unless you're Dutch.
        Now is better than never.
        Although never is often better than *right* now.
        If the implementation is hard to explain, it's a bad idea.
        If the implementation is easy to explain, it may be a good idea.
        Namespaces are one honking great idea -- let's do more of those!'''
    serv.sendMessage(msg, to=[serv.addrTo])
