#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_comm.dev"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_comm.notification
import pyauto_comm.web
import pyauto_comm.dev

class Test_dev(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_Github(self):
    try:
      serv = pyauto_comm.dev.Github()
    except pyauto_comm.web.WebServiceException:
      self.skipTest("")
      serv = None

    logger.info(serv)
    res = serv.getUser()
    logger.info(json.dumps(res, indent=2))
    self.assertEqual(res["type"], "User")
    self.assertGreaterEqual(res["public_repos"], 0)
    self.assertGreaterEqual(res["disk_usage"], 0)

    res = serv.getOrg(None)
    logger.info(json.dumps(res, indent=2))
