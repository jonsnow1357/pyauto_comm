#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_comm.IoT"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_comm.web
import pyauto_comm.IoT

class Test_IoT(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_ThingSpeak(self):
    try:
      serv = pyauto_comm.IoT.ThingSpeak()
    except pyauto_comm.web.WebServiceException:
      self.skipTest("")
      serv = None

    logger.info(serv)
    res = serv.getChannelList()
    logger.info(json.dumps(res, indent=2))
    self.assertEqual(res[0]["name"], "test")

  #@unittest.skip("")
  def test_Adafruit(self):
    try:
      serv = pyauto_comm.IoT.Adafruit()
    except pyauto_comm.web.WebServiceException:
      self.skipTest("")
      serv = None

    logger.info(serv)
    res = serv.getUser()
    #print("DBG", res.keys())
    logger.info("== user ==\n{}".format(json.dumps(res["user"], indent=2)))
    logger.info("== system_messages ==\n{}".format(
        json.dumps(res["system_messages"], indent=2)))

  #@unittest.skip("")
  def test_AdafruitMQTT(self):
    try:
      import paho.mqtt.client
    except ImportError:
      self.skipTest("")

    try:
      serv = pyauto_comm.IoT.AdafruitMQTT()
    except pyauto_comm.web.WebServiceException:
      self.skipTest("")
      serv = None

    logger.info(serv)
    serv.connect()
    pyauto_base.misc.waitWithPrint(5)
    serv.disconnect()
