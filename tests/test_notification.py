#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_comm.notification"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_comm.web
import pyauto_comm.notification

class Test_notification(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  @unittest.skip("")
  def test_Twitter(self):
    try:
      serv = pyauto_comm.notification.Twitter()
    except pyauto_comm.web.WebServiceException:
      self.skipTest("")
      serv = None

    logger.info(serv)
    serv.showUserInfo()
    serv.getTimeline()

  #@unittest.skip("")
  def test_Mastodon(self):
    try:
      serv = pyauto_comm.notification.Mastodon()
    except pyauto_comm.web.WebServiceException:
      self.skipTest("")
      serv = None

    logger.info(serv)
    serv.showUserInfo()
    serv.getTimeline()

  #@unittest.skip("")
  def test_Pushover(self):
    try:
      serv = pyauto_comm.notification.Pushover()
    except pyauto_comm.web.WebServiceException:
      self.skipTest("")
      serv = None

    logger.info(serv)
    serv.sendMessage("test message", bSend=False, device="device")
    serv.sendMessage("test message", bSend=False)
    #serv.sendMessage("test message")

  #@unittest.skip("")
  def test_Slack(self):
    try:
      serv = pyauto_comm.notification.Slack()
    except pyauto_comm.web.WebServiceException:
      self.skipTest("")
      serv = None

    logger.info(serv)
