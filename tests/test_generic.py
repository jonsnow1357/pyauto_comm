#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_comm.generic"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import typing
import unittest
import random
import serial
import socket

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_base.config
import pyauto_comm.generic

def _read_connections(fPath):
  cfg = pyauto_base.config.SimpleConfig()
  cfg.loadCfgFile(fPath)

  res = {}
  for k, v in cfg.default.items():
    if (k.startswith("conn_")):
      res[k[5:]] = v
  return res

def _getRndConnectionInfo():
  tmp = random.randint(0, 9)
  if (tmp < 2):
    user = ""
    pswd = ""
  elif (tmp < 6):
    user = pyauto_base.misc.getRndStr(8)
    pswd = ""
  else:
    user = pyauto_base.misc.getRndStr(8)
    pswd = pyauto_base.misc.getRndStr(8)

  tmp = random.randint(0, 9)
  if (tmp < 5):
    host = "127.0.0.1"
  else:
    host = "example.com"

  tmp = random.randint(0, 9)
  if (tmp < 2):
    port = ""
  else:
    port = random.randint(1000, 10000)

  tmp = random.randint(0, 9)
  if (tmp < 2):
    path = ""
  else:
    path = "/" + "/".join([pyauto_base.misc.getRndStr(8), pyauto_base.misc.getRndStr(8)])
  return [user, pswd, host, port, path]

def _mkConnectionString(proto, user, pswd, host, port):
  if (pyauto_base.misc.isEmptyString(host)):
    raise NotImplementedError

  res = proto
  if (proto != ""):
    res += "://"

  if ((user != "") or (pswd != "")):
    if (user != ""):
      res += user
    if (pswd != ""):
      res += (":" + pswd)
    res += "@"

  res += host

  if (isinstance(port, int)):
    res += (":" + str(port))
  elif (port != ""):
    res += (":" + port)
  return res

class Test_CommInterfaceInfo(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  def _testCommInterfaceInfo(self, if_info, dictVal):
    self.assertEqual(if_info.type, dictVal["type"])
    self.assertEqual(if_info.host, dictVal["host"])
    self.assertEqual(if_info.port, dictVal["port"])
    self.assertEqual(if_info.URL, dictVal["URL"])
    self.assertEqual(if_info.user, dictVal["user"])
    self.assertEqual(if_info.pswd, dictVal["pswd"])
    self.assertEqual(if_info.dev, dictVal["dev"])
    self.assertEqual(if_info.speed, dictVal["speed"])
    self.assertEqual(if_info.timeout, dictVal["timeout"])

  #@unittest.skip("")
  def test_parse_Socket(self):
    for _ in range(10):
      user, pswd, host, port, path = _getRndConnectionInfo()
      if (port == ""):
        continue
      strConnInfo = _mkConnectionString("", "", "", host, port)
      logger.info("conn info: {}".format(strConnInfo))
      ifInfo = pyauto_comm.generic.CommInterfaceInfo(strConnInfo)
      dictIfInfo = {
          "type": pyauto_comm.generic.ifSocket,
          "host": host,
          "port": port,
          "URL": None,
          "user": None,
          "pswd": None,
          "dev": None,
          "speed": None,
          "timeout": pyauto_comm.generic.timeout_net
      }
      self._testCommInterfaceInfo(ifInfo, dictIfInfo)
      logger.info(ifInfo)
      logger.info(ifInfo.DBG)

  #@unittest.skip("")
  def test_parse_Telnet(self):
    for _ in range(10):
      user, pswd, host, port, path = _getRndConnectionInfo()
      if (port == ""):
        strConnInfo = _mkConnectionString("", "", "", host, "")
        user = None
        pswd = None
        port = 23
      else:
        strConnInfo = _mkConnectionString("telnet", user, pswd, host, port)
      logger.info("conn info: {}".format(strConnInfo))
      ifInfo = pyauto_comm.generic.CommInterfaceInfo(strConnInfo)
      dictIfInfo = {
          "type": pyauto_comm.generic.ifTelnet,
          "host": host,
          "port": port,
          "URL": None,
          "user": user,
          "pswd": pswd,
          "dev": None,
          "speed": None,
          "timeout": pyauto_comm.generic.timeout_net
      }
      self._testCommInterfaceInfo(ifInfo, dictIfInfo)
      logger.info(ifInfo)
      logger.info(ifInfo.DBG)

  #@unittest.skip("")
  def test_parse_SSH(self):
    for _ in range(10):
      user, pswd, host, port, path = _getRndConnectionInfo()
      strConnInfo = _mkConnectionString("ssh", user, pswd, host, port)
      logger.info("conn info: {}".format(strConnInfo))
      ifInfo = pyauto_comm.generic.CommInterfaceInfo(strConnInfo)
      dictIfInfo = {
          "type": pyauto_comm.generic.ifSSH,
          "host": host,
          "port": 22 if (port == "") else port,
          "URL": None,
          "user": user,
          "pswd": pswd,
          "dev": None,
          "speed": None,
          "timeout": pyauto_comm.generic.timeout_net
      }
      self._testCommInterfaceInfo(ifInfo, dictIfInfo)
      logger.info(ifInfo)
      logger.info(ifInfo.DBG)

  #@unittest.skip("")
  def test_parse_HTTP(self):
    for _ in range(10):
      user, pswd, host, port, path = _getRndConnectionInfo()
      strConnInfo = _mkConnectionString("http", "", "", host, port) + path
      logger.info("conn info: {}".format(strConnInfo))
      ifInfo = pyauto_comm.generic.CommInterfaceInfo(strConnInfo)
      dictIfInfo = {
          "type": pyauto_comm.generic.ifHTTP,
          "host": host,
          "port": 80 if (port == "") else port,
          "URL": strConnInfo,
          "user": None,
          "pswd": None,
          "dev": None,
          "speed": None,
          "timeout": pyauto_comm.generic.timeout_net
      }
      self._testCommInterfaceInfo(ifInfo, dictIfInfo)
      logger.info(ifInfo)
      #logger.info(ifInfo.DBG)

  #@unittest.skip("")
  def test_parse_HTTPS(self):
    for _ in range(10):
      user, pswd, host, port, path = _getRndConnectionInfo()
      strConnInfo = _mkConnectionString("https", "", "", host, port) + path
      logger.info("conn info: {}".format(strConnInfo))
      ifInfo = pyauto_comm.generic.CommInterfaceInfo(strConnInfo)
      dictIfInfo = {
          "type": pyauto_comm.generic.ifHTTPS,
          "host": host,
          "port": 443 if (port == "") else port,
          "URL": strConnInfo,
          "user": None,
          "pswd": None,
          "dev": None,
          "speed": None,
          "timeout": pyauto_comm.generic.timeout_net
      }
      self._testCommInterfaceInfo(ifInfo, dictIfInfo)
      logger.info(ifInfo)
      #logger.info(ifInfo.DBG)

  #@unittest.skip("")
  def test_parse_SNMP(self):
    for _ in range(10):
      user, pswd, host, port, path = _getRndConnectionInfo()
      strConnInfo = _mkConnectionString("snmp", user, pswd, host, port)
      logger.info("conn info: {}".format(strConnInfo))
      ifInfo = pyauto_comm.generic.CommInterfaceInfo(strConnInfo)
      dictIfInfo = {
          "type": pyauto_comm.generic.ifSNMP,
          "host": host,
          "port": 161 if (port == "") else port,
          "URL": None,
          "user": user,
          "pswd": pswd,
          "dev": None,
          "speed": None,
          "timeout": pyauto_comm.generic.timeout_net
      }
      self._testCommInterfaceInfo(ifInfo, dictIfInfo)
      logger.info(ifInfo)
      logger.info(ifInfo.DBG)

  #@unittest.skip("")
  def test_parse_FTP(self):
    for _ in range(10):
      user, pswd, host, port, path = _getRndConnectionInfo()
      strConnInfo = _mkConnectionString("ftp", user, pswd, host, port)
      logger.info("conn info: {}".format(strConnInfo))
      ifInfo = pyauto_comm.generic.CommInterfaceInfo(strConnInfo)
      dictIfInfo = {
          "type": pyauto_comm.generic.ifFTP,
          "host": host,
          "port": 21 if (port == "") else port,
          "URL": None,
          "user": user,
          "pswd": pswd,
          "dev": None,
          "speed": None,
          "timeout": pyauto_comm.generic.timeout_FTP
      }
      self._testCommInterfaceInfo(ifInfo, dictIfInfo)
      logger.info(ifInfo)
      logger.info(ifInfo.DBG)

  #@unittest.skip("")
  def test_parse_FTPS(self):
    for _ in range(10):
      user, pswd, host, port, path = _getRndConnectionInfo()
      strConnInfo = _mkConnectionString("ftps", user, pswd, host, port)
      logger.info("conn info: {}".format(strConnInfo))
      ifInfo = pyauto_comm.generic.CommInterfaceInfo(strConnInfo)
      dictIfInfo = {
          "type": pyauto_comm.generic.ifFTPS,
          "host": host,
          "port": 21 if (port == "") else port,
          "URL": None,
          "user": user,
          "pswd": pswd,
          "dev": None,
          "speed": None,
          "timeout": pyauto_comm.generic.timeout_FTP
      }
      self._testCommInterfaceInfo(ifInfo, dictIfInfo)
      logger.info(ifInfo)
      logger.info(ifInfo.DBG)

  #@unittest.skip("")
  def test_parse_UART(self):
    for _ in range(10):
      dev = "COM{}".format(random.randint(1, 10))
      speed = random.choice([""] + list(pyauto_comm.generic.UARTSpeed))
      if (speed == ""):
        strConnInfo = dev
      else:
        strConnInfo = "{}:{}".format(dev, speed)
      logger.info("conn info: {}".format(strConnInfo))
      ifInfo = pyauto_comm.generic.CommInterfaceInfo(strConnInfo)
      dictIfInfo = {
          "type": pyauto_comm.generic.ifUART,
          "host": None,
          "port": None,
          "URL": None,
          "user": None,
          "pswd": None,
          "dev": dev,
          "speed": 115200 if (speed == "") else speed,
          "timeout": pyauto_comm.generic.timeout_UART
      }
      self._testCommInterfaceInfo(ifInfo, dictIfInfo)
      logger.info(ifInfo)
      logger.info(ifInfo.DBG)

    for _ in range(10):
      dev = "{}{}".format(random.choice(["/dev/tty", "/dev/ttyUSB", "/dev/ttyACM"]),
                          random.randint(1, 10))
      speed = random.choice([""] + list(pyauto_comm.generic.UARTSpeed))
      if (speed == ""):
        strConnInfo = dev
      else:
        strConnInfo = "{}:{}".format(dev, speed)
      logger.info("conn info: {}".format(strConnInfo))
      ifInfo = pyauto_comm.generic.CommInterfaceInfo(strConnInfo)
      dictIfInfo = {
          "type": pyauto_comm.generic.ifUART,
          "host": None,
          "port": None,
          "URL": None,
          "user": None,
          "pswd": None,
          "dev": dev,
          "speed": 115200 if (speed == "") else speed,
          "timeout": pyauto_comm.generic.timeout_UART
      }
      self._testCommInterfaceInfo(ifInfo, dictIfInfo)
      logger.info(ifInfo)
      logger.info(ifInfo.DBG)

  #@unittest.skip("")
  def test_parse_VISA(self):
    for url in ("ASRL::1.2.3.4::2::INSTR", "ASRL1::INSTR", "GPIB::1::0::INSTR",
                "GPIB2::INTFC", "PXI::15::INSTR", "PXI::2::BACKPLANE",
                "PXI::CHASSIS1::SLOT3", "PXI0::2-12.1::INSTR", "PXI0::MEMACC",
                "TCPIP::example.com::INSTR", "TCPIP0::1.2.3.4::999::SOCKET",
                "USB::0x1234::125::A22-5::INSTR", "USB::0x5678::0x33::SN999::1::RAW",
                "VXI::1::BACKPLANE", "VXI::MEMACC", "VXI0::1::INSTR", "VXI0::SERVANT"):
      logger.info("conn info: {}".format(url))
      ifInfo = pyauto_comm.generic.CommInterfaceInfo(url)
      dictIfInfo = {
          "type": pyauto_comm.generic.ifVISA,
          "host": None,
          "port": None,
          "URL": url,
          "user": None,
          "pswd": None,
          "dev": None,
          "speed": None,
          "timeout": pyauto_comm.generic.timeout_net
      }
      self._testCommInterfaceInfo(ifInfo, dictIfInfo)
      logger.info(ifInfo)
      #logger.info(ifInfo.DBG)

class Test_CommInterface_Socket(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    fPath = os.path.join(self.inFolder, "connections.ini")
    dictConn = _read_connections(fPath)
    cif = pyauto_comm.generic.CommInterface(dictConn["socket"])
    logger.info(cif)

    self.assertFalse(cif.isActive())
    cif.open()
    self.assertTrue(cif.isActive())

    t0 = datetime.datetime.now()
    res = cif.communicate("GET / HTTP/1.1\r\n\r\n", "", TOmultiplier=4)
    self.assertGreaterEqual(len(res), 1)
    if (len(res) > 256):
      logger.info("reply TOO BIG")
    else:
      for ln in res:
        logger.info(ln)
    runTime = datetime.datetime.now() - t0
    logger.info("time: {}".format(runTime))
    self.assertTrue(runTime.total_seconds() >= 4)

    self.assertTrue(cif.isActive())
    cif.close()
    self.assertFalse(cif.isActive())

  #@unittest.skip("")
  def test_local(self):
    fPath = os.path.join(self.inFolder, "connections.ini")
    dictConn = _read_connections(fPath)
    cif = pyauto_comm.generic.CommInterface(dictConn["socket_lcl"])
    logger.info(cif)

    nTry = 64
    sLen = 256
    for i in range(nTry):
      try:
        cif.open()
      except pyauto_comm.generic.CommException as ex:
        self.skipTest("host NOT available")

      tmp = pyauto_base.misc.getRndStr(sLen)
      res = cif.query(tmp)
      self.assertEqual(res[0], tmp)

      cif.close()
      if ((i % 32) == 31) or (i == (nTry - 1)):
        logger.info("sent {:4d} queries".format(i + 1))

class Test_CommInterface_Telnet(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  @unittest.skip("")
  def test_base(self):
    fPath = os.path.join(self.inFolder, "connections.ini")
    dictConn = _read_connections(fPath)
    cif = pyauto_comm.generic.CommInterface(dictConn["telnet"])
    logger.info(cif)

    self.assertFalse(cif.isActive())
    cif.open()
    self.assertTrue(cif.isActive())

    t0 = datetime.datetime.now()
    res = cif.communicate("", "", TOmultiplier=4)
    for ln in res:
      logger.info(ln)
    self.assertGreaterEqual(len(res), 18)
    self.assertEqual(res[-1], ".")
    runTime = datetime.datetime.now() - t0
    logger.info("time: {}".format(runTime))
    self.assertTrue(runTime.total_seconds() >= 4)

    t0 = datetime.datetime.now()
    res = cif.communicate("?" + cif.eos, "\r\n.", TOmultiplier=4)
    for ln in res:
      logger.info(ln)
    self.assertGreater(len(res), 18)
    self.assertEqual(res[-1], ".")
    runTime = datetime.datetime.now() - t0
    logger.info("time: {}".format(runTime))
    self.assertTrue(runTime.total_seconds() < 4)

    self.assertTrue(cif.isActive())
    cif.close()
    self.assertFalse(cif.isActive())

  @unittest.skip("")
  def test_local(self):
    fPath = os.path.join(self.inFolder, "connections.ini")
    dictConn = _read_connections(fPath)
    cif = pyauto_comm.generic.CommInterface(dictConn["telnet_lcl"])
    logger.info(cif)

    nTry = 64
    for i in range(nTry):
      try:
        cif.open()
      except pyauto_comm.generic.CommException as ex:
        self.skipTest("host NOT available")

      cif.eom = ": "
      res = cif.read(TOmultiplier=2)
      #print("DBG", res)
      self.assertEqual(len(res), 2, res)
      self.assertTrue(res[-1].endswith("login: "))
      res = cif.query(cif.info.user, TOmultiplier=2)
      #print("DBG", res)
      self.assertEqual(len(res), 2, res)
      self.assertEqual(res[-1], "Password: ")
      cif.eom = "$ "
      res = cif.query(cif.info.pswd, TOmultiplier=8)
      #print("DBG", res)
      self.assertGreaterEqual(len(res), 5, res)
      self.assertTrue(res[1].startswith("Linux"))

      res = cif.query("cat /proc/cpuinfo", TOmultiplier=2)
      #print("DBG", res)
      self.assertEqual(len(res), 14, res)
      self.assertEqual(res[2], "model name\t: ARMv6-compatible processor rev 7 (v6l)")

      res = cif.query("cat /proc/meminfo", TOmultiplier=2)
      #print("DBG", res)
      self.assertEqual(len(res), 39, res)
      self.assertEqual(res[1], "MemTotal:         443084 kB")

      res = cif.query("cat /proc/partitions", TOmultiplier=2)
      #print("DBG", res)
      self.assertEqual(len(res), 22, res)
      self.assertEqual(res[2], "   1        0       4096 ram0")

      cif.close()
      pyauto_base.misc.waitWithPrint(1)
      if ((i % 32) == 31) or (i == (nTry - 1)):
        logger.info("sent {:4d} queries".format(i + 1))

class Test_CommInterface_SSH(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    fPath = os.path.join(self.inFolder, "connections.ini")
    dictConn = _read_connections(fPath)
    cif = pyauto_comm.generic.CommInterface(dictConn["ssh"])
    logger.info(cif)

    self.assertFalse(cif.isActive())
    cif.open()
    self.assertTrue(cif.isActive())

    t0 = datetime.datetime.now()
    res = cif.readRaw("")
    for ln in res:
      logger.info(pyauto_base.misc.convertStringNonPrint(ln))
    runTime = datetime.datetime.now() - t0
    logger.info("time: {}".format(runTime))

    t0 = datetime.datetime.now()
    res = cif.communicate("s", "", TOmultiplier=4)
    for ln in res:
      logger.info(pyauto_base.misc.convertStringNonPrint(ln))
    self.assertEqual(len(res), 14)
    self.assertEqual(res[-1], "\x1b[1B  => ")
    runTime = datetime.datetime.now() - t0
    logger.info("time: {}".format(runTime))
    self.assertTrue(runTime.total_seconds() >= 4)

    t0 = datetime.datetime.now()
    res = cif.communicate("q", "=> ", TOmultiplier=4)
    for ln in res:
      logger.info(pyauto_base.misc.convertStringNonPrint(ln))
    self.assertEqual(len(res), 9)
    self.assertEqual(res[-1], "\x1b[4B  => ")
    runTime = datetime.datetime.now() - t0
    logger.info("time: {}".format(runTime))
    self.assertTrue(runTime.total_seconds() < 4)

    self.assertTrue(cif.isActive())
    cif.close()
    self.assertFalse(cif.isActive())

  #@unittest.skip("")
  def test_local(self):
    fPath = os.path.join(self.inFolder, "connections.ini")
    dictConn = _read_connections(fPath)
    cif = pyauto_comm.generic.CommInterface(dictConn["ssh_lcl"])
    logger.info(cif)

    nTry = 64
    for i in range(nTry):
      try:
        cif.open()
      except pyauto_comm.generic.CommException as ex:
        self.skipTest("host NOT available")

      cif.eom = "$ "
      cif.eom_cnt = 1
      res = cif.read(TOmultiplier=4)
      #print("DBG", res)
      self.assertGreaterEqual(len(res), 5, res)
      self.assertTrue(res[0].startswith("Linux"))
      cif.eom_cnt = 2

      res = cif.query("cat /proc/cpuinfo", TOmultiplier=2)
      #print("DBG", res)
      self.assertEqual(len(res), 15, res)
      self.assertEqual(res[2], "model name\t: ARMv6-compatible processor rev 7 (v6l)")

      res = cif.query("cat /proc/meminfo", TOmultiplier=2)
      #print("DBG", res)
      self.assertEqual(len(res), 40, res)
      self.assertEqual(res[1], "MemTotal:         443084 kB")

      res = cif.query("cat /proc/partitions", TOmultiplier=2)
      #print("DBG", res)
      self.assertEqual(len(res), 23, res)
      self.assertEqual(res[2], "   1        0       4096 ram0")

      cif.close()
      pyauto_base.misc.waitWithPrint(1)
      if ((i % 32) == 31) or (i == (nTry - 1)):
        logger.info("sent {:4d} queries".format(i + 1))

class Test_CommInterface_HTTP(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    fPath = os.path.join(self.inFolder, "connections.ini")
    dictConn = _read_connections(fPath)
    cif = pyauto_comm.generic.CommInterface(dictConn["http"])
    cif.timeout = 4
    logger.info(cif)

    self.assertFalse(cif.isActive())
    cif.open()
    self.assertTrue(cif.isActive())

    res = cif.get(url="/anything")
    cnt = 0
    for ln in res.split("\n"):
      logger.info("<< {}".format(ln))
      cnt += len(ln)
    self.assertGreater(cnt, 0)

    res = cif.post(url="/anything")
    cnt = 0
    for ln in res.split("\n"):
      logger.info("<< {}".format(ln))
      cnt += len(ln)
    self.assertGreater(cnt, 0)

    res = cif.put(url="/anything")
    cnt = 0
    for ln in res.split("\n"):
      logger.info("<< {}".format(ln))
      cnt += len(ln)
    self.assertGreater(cnt, 0)

    self.assertTrue(cif.isActive())
    cif.close()
    self.assertFalse(cif.isActive())

class Test_CommInterface_HTTPS(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    fPath = os.path.join(self.inFolder, "connections.ini")
    dictConn = _read_connections(fPath)
    cif = pyauto_comm.generic.CommInterface(dictConn["https"])
    cif.timeout = 4
    logger.info(cif)

    self.assertFalse(cif.isActive())
    cif.open()
    self.assertTrue(cif.isActive())

    res = cif.get(url="/anything")
    cnt = 0
    for ln in res.split("\n"):
      logger.info("<< {}".format(ln))
      cnt += len(ln)
    self.assertGreater(cnt, 0)

    res = cif.post(url="/anything")
    cnt = 0
    for ln in res.split("\n"):
      logger.info("<< {}".format(ln))
      cnt += len(ln)
    self.assertGreater(cnt, 0)

    res = cif.put(url="/anything")
    cnt = 0
    for ln in res.split("\n"):
      logger.info("<< {}".format(ln))
      cnt += len(ln)
    self.assertGreater(cnt, 0)

    self.assertTrue(cif.isActive())
    cif.close()
    self.assertFalse(cif.isActive())

class Test_CommInterface_SNMP(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  @unittest.skip("")
  def test_base(self):
    try:
      import pysnmp
    except ImportError:
      logger.warning("pysnmp is not installed")
      return

    if (not hasattr(pysnmp, "version")):
      logger.error("pysnmp is too old ... skipping test")
      return
    logger.info("pysnmp ver: {}".format(pysnmp.version))

    fPath = os.path.join(self.inFolder, "connections.ini")
    dictConn = _read_connections(fPath)
    cif = pyauto_comm.generic.CommInterface(dictConn["snmp"])
    logger.info(cif)

    self.assertFalse(cif.isActive())
    cif.open()
    self.assertTrue(cif.isActive())

    t0 = datetime.datetime.now()
    res = cif.get(key="1.3.6.1.2.1.1.1.0")
    for ln in res:
      logger.info(ln)
    if (sys.version_info[0] == 2):
      self.assertRegexpMatches(res[1], "(Linux|SunOS|Samsung).*")
    else:
      self.assertRegex(res[1], "(Linux|SunOS|Samsung).*")
    runTime = datetime.datetime.now() - t0
    logger.info("time: {}".format(runTime))

    #tmp = pyauto_base.misc.getRndStr(32)
    #cif.set(key="1.3.6.1.2.1.1.9.1.3.1", val=tmp)
    #res = cif.get(key)
    #self.assertEqual(res[1], tmp)

    self.assertTrue(cif.isActive())
    cif.close()
    self.assertFalse(cif.isActive())

class Test_CommInterface_FTP(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    fPath = os.path.join(self.inFolder, "connections.ini")
    dictConn = _read_connections(fPath)
    cif = pyauto_comm.generic.CommInterface(dictConn["ftp"])
    logger.info(cif)

    self.assertFalse(cif.isActive())
    try:
      cif.open()
    except pyauto_comm.generic.CommException as ex:
      self.skipTest("host NOT available")
    self.assertTrue(cif.isActive())

    res = cif.ls()
    logger.info("LS: {}".format(res))

    cif.putFile(os.path.join(self.inFolder, "1k.iso"), "1k.iso")

    f_path = os.path.join(self.outFolder, "1k.iso")
    cif.getFile("1k.iso", f_path)

    hashVal = [
        "5f70bf18a086007016e948b04aed3b82103a36bea41755b6cddfaf10ace3c6ef",
    ]
    self.assertTrue(os.path.isfile(f_path), "file DOES NOT exist: {}".format(f_path))
    tmp = pyauto_base.misc.getFileHash(f_path)
    self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(f_path, tmp))

    self.assertTrue(cif.isActive())
    cif.close()
    self.assertFalse(cif.isActive())

class Test_CommInterface_UART(unittest.TestCase):
  cwd = ""
  lclDir = ""
  lstPorts: list[str] = []
  lstLoop: list[str] = []

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test01_scan(self):
    logger.info("scanning UART ports ...")
    if (sys.platform.startswith("win")):
      for i in range(32):
        try:
          name = "COM{}".format(i)
          prt = serial.Serial(name)
          prt.close()
          self.lstPorts.append(name)
        except serial.SerialException:
          pass
    elif (sys.platform.startswith("linux")):
      for i in range(4):
        try:
          name = "/dev/ttyUSB{}".format(i)
          prt = serial.Serial(name)
          prt.close()
          self.lstPorts.append(name)
          name = "/dev/ttyACM{}".format(i)
          prt = serial.Serial(name)
          prt.close()
          self.lstPorts.append(name)
        except serial.SerialException:
          pass

    logger.info("found {} ports".format(len(self.lstPorts)))

  #@unittest.skip("")
  def test02_create_connection(self):
    for dev in self.lstPorts:
      cif = pyauto_comm.generic.CommInterface("{}:115200".format(dev))

      self.assertFalse(cif.isActive())
      cif.open()
      self.assertTrue(cif.isActive())

      tmp1 = pyauto_base.misc.getRndStr(4)
      try:
        cif.writeRaw(tmp1)
        tmp2 = cif.readRaw("")
        if (len(tmp2) > 0):
          if (tmp1 == tmp2[0]):
            logger.info("loopback found on {}".format(cif))
            self.lstLoop.append(dev)
          else:
            logger.warning("NO loopback found on {}".format(cif))
        else:
          logger.warning("NO loopback found on {}".format(cif))
      except serial.SerialTimeoutException:
        logger.warning("NO loopback found on {} (timeout)".format(cif))

      self.assertTrue(cif.isActive())
      cif.close()
      self.assertFalse(cif.isActive())

  #@unittest.skip("")
  def test03_use_connection(self):
    for dev in self.lstLoop:
      for spd in pyauto_comm.generic.UARTSpeed:
        cif = pyauto_comm.generic.CommInterface("{}:{}".format(dev, spd))

        self.assertFalse(cif.isActive())
        cif.open()
        self.assertTrue(cif.isActive())

        tmp1 = pyauto_base.misc.getRndStr(128)
        cif.writeRaw(tmp1)
        res = cif.readRaw("")
        self.assertEqual(tmp1, res[0])

        tmp1 = pyauto_base.misc.getRndStr(128)
        res = cif.communicate(tmp1)
        self.assertEqual(tmp1, res[0])

        self.assertTrue(cif.isActive())
        cif.close()
        self.assertFalse(cif.isActive())
