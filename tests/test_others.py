#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_comm.dev"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
import json

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_comm.notification
import pyauto_comm.web
import pyauto_comm.others

class Test_others(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_HIBP(self):
    try:
      serv = pyauto_comm.others.HIBP()
    except pyauto_comm.web.WebServiceException:
      self.skipTest("")
      serv = None

    logger.info(serv)
    res = serv.getAllBreaches()
    for br in res:
      #logger.info(br)
      logger.info("{}({}) {} {} {}".format(br["Name"], br["Domain"], br["BreachDate"],
                                           br["PwnCount"], br["DataClasses"]))
