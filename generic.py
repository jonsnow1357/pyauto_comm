#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library for communication interfaces"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import typing
import socket
import requests
import urllib.parse

logger = logging.getLogger("lib")
logcomms = logging.getLogger("comms")
import pyauto_base.fs
import pyauto_base.misc

ifSSH = "ssh"
ifTelnet = "telnet"
ifSocket = "socket"
ifFTP = "ftp"
ifFTPS = "ftps"
ifSNMP = "snmp"
ifHTTP = "http"
ifHTTPS = "https"
ifUART = "UART"
ifVISA = "VISA"  # Virtual Instrument System Architecture
ifTypes_Net = (ifSSH, ifTelnet, ifSocket, ifFTP, ifFTPS, ifSNMP, ifHTTP, ifHTTPS)
ifTypes_Web = (ifHTTP, ifHTTPS)
ifTypes_All = ifTypes_Net + (ifUART, ifVISA)
UARTSpeed = (9600, 19200, 38400, 57600, 115200)
regexIP = r"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\." \
          r"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
regexSocket = r"^[A-Za-z0-9_\-\.]+(:[0-9]*)?$"
regexUART = r"^(COM|\/dev\/tty)([^:]+)(:[0-9]+)?$"
regexVISA = r"^(ASRL|GPIB|PXI|TCPIP|USB|VXI)[0-9]*::.*$"
timeout_net = 1.0
timeout_FTP = 5.0
timeout_UART = 1.0

class CommException(Exception):
  pass

class CommTimeoutException(CommException):
  pass

class CommInterfaceInfo(object):

  def __init__(self, strVal: str):
    self._type: typing.Optional[str] = None
    self._value = strVal
    self._name: typing.Optional[str] = None
    self._host: typing.Optional[str] = None  # IPv4, IPv6, FQDN, hostname
    self.port: typing.Optional[int] = None
    self.user: typing.Optional[str] = None
    self.pswd: typing.Optional[str] = None
    self.SNMP_ro = "public"
    self.SNMP_rw = "private"
    self.SNMP_trap = "private"
    self.dev: typing.Optional[str] = None
    self.speed: typing.Optional[int] = None
    self.timeout = timeout_net
    self.eos = "\r\n"  # End of String, added after each sent message, seems to work for telnet and UART
    self.eom = ""  # End of Message, expected after each received message
    self.eom_cnt = 1  # sometimes CLIs return multiple prompts (End of Message)

    self._parseValue()

  @property
  def DBG(self) -> str:
    if (self._type in list(set(ifTypes_Net) - set(ifTypes_Web))):
      if (self.user):
        return "{}::{}@{}".format(self.type.upper(), self.user, self.host)
      else:
        return "{}::{}".format(self.type.upper(), self.host)
    elif (self._type == ifUART):
      return "{}::{}".format(self._type.upper(), self.dev)
    elif (self._type == ifVISA):
      return "{}::{}".format(self._type.upper(), self.URL.split("::")[0])
    else:
      msg = "UNEXPECTED {} type: {}".format(self.__class__.__name__, self._type)
      logger.error(msg)
      raise CommException(msg)

  @property
  def INFO(self) -> str:
    if (self._type in list(set(ifTypes_Net) - set(ifTypes_Web))):
      if (self.user == ""):
        return "{}://{}:{}".format(self._type, self._host, self.port)
      else:
        return "{}://{}@{}:{}".format(self._type, self.user, self._host, self.port)
    elif (self._type in ifTypes_Web):
      return "{}://{}:{}".format(self._type, self._host, self.port)
    elif (self._type == ifUART):
      return "{}://{}:{:d}bps".format(self._type, self.dev, self.speed)
    elif (self._type == ifVISA):
      return self.URL
    else:
      msg = "UNEXPECTED {} type: {}".format(self.__class__.__name__, self._type)
      logger.error(msg)
      raise CommException(msg)

  def __str__(self):
    return "{} {}".format(self.__class__.__name__, self.INFO)

  @property
  def type(self) -> typing.Optional[str]:
    return self._type

  @property
  def value(self) -> str:
    return self._value

  @property
  def host(self) -> typing.Optional[str]:
    return self._host

  @property
  def URL(self) -> typing.Optional[str]:
    if (self._type in (ifHTTP, ifHTTPS, ifVISA)):
      return self._value
    else:
      return None

  def _parseValue_socket(self) -> None:
    tmp = self._value.split(":")
    if (len(tmp) == 1):
      self._host = tmp[0]
      self.port = 23  # assume telnet if no port
      self._type = ifTelnet
    elif (len(tmp) == 2):
      self._host = tmp[0]
      self.port = int(tmp[1])
      self._type = ifSocket
    else:
      msg = "INCORRECT {} value: {}".format(self.__class__.__name__, self._value)
      logger.error(msg)
      raise CommException(msg)

  def _parseValue_protocol(self) -> None:
    if (self._value.endswith("/")):
      res = pyauto_base.misc.parseProtocolString(self._value)
    else:
      res = pyauto_base.misc.parseProtocolString(self._value + "/")

    if (not pyauto_base.misc.isEmptyString(res["path"])):
      raise NotImplementedError

    self._host = res["hostname"]
    self.port = int(res["port"]) if (res["port"] is not None) else None
    if (res["user"] is None):
      self.user = ""
    else:
      self.user = res["user"]
    if (res["pswd"] is None):
      self.pswd = ""
    else:
      self.pswd = res["pswd"]

  def _parseValue_URL(self) -> None:
    url_parts = urllib.parse.urlsplit(self._value)

    res = url_parts.netloc.split(":")
    if (len(res) == 1):
      self._host = res[0]
    elif (len(res) == 2):
      self._host = res[0]
      self.port = int(res[1]) if (res[1] is not None) else None
    else:
      raise NotImplementedError

  def _parseValue_UART(self) -> None:
    tmp = self._value.split(":")
    if (len(tmp) == 1):
      self.dev = tmp[0]
      self.speed = 115200
    elif (len(tmp) == 2):
      self.dev = tmp[0]
      self.speed = int(tmp[1])
    else:
      msg = "INCORRECT {} value: {}".format(self.__class__.__name__, self._value)
      logger.error(msg)
      raise CommException(msg)

    self._type = ifUART
    self.timeout = timeout_UART

  def _parseValue_VISA(self) -> None:
    self._type = ifVISA

  def _parseValue(self) -> None:
    """
    parse something like:
      ip[:port]
      hostname[:port]
      devname[:rate]
      protocol://ip[:port]
      protocol://url[:port]
      protocol://user[:password]@ip[:port]
      protocol://user[:password]@url[:port]
    """
    if (sys.version_info[0] == 2):
      if (isinstance(self._value, unicode)):
        self._value = str(self._value)
    if (not isinstance(self._value, str)):
      msg = "INCORRECT type for value: {}".format(type(self._value))
      logger.error(msg)
      raise CommException(msg)

    #print("DBG", self._value)
    if (self._value.startswith("ssh://")):
      self._parseValue_protocol()
      if (self.port is None):
        self.port = 22
      else:
        self.port = int(self.port)
      self._type = ifSSH
    elif (self._value.startswith("telnet://")):
      self._parseValue_protocol()
      if (self.port is None):
        self.port = 23
      else:
        self.port = int(self.port)
      self._type = ifTelnet
    elif (self._value.startswith("http://")):
      self._parseValue_URL()
      if (self.port is None):
        self.port = 80
      else:
        self.port = int(self.port)
      self._type = ifHTTP
    elif (self._value.startswith("https://")):
      self._parseValue_URL()
      if (self.port is None):
        self.port = 443
      else:
        self.port = int(self.port)
      self._type = ifHTTPS
    elif (self._value.startswith("ftp://")):
      self._parseValue_protocol()
      if (self.port is None):
        self.port = 21
      else:
        self.port = int(self.port)
      self._type = ifFTP
      self.timeout = timeout_FTP
    elif (self._value.startswith("ftps://")):
      self._parseValue_protocol()
      if (self.port is None):
        self.port = 21
      else:
        self.port = int(self.port)
      self._type = ifFTPS
      self.timeout = timeout_FTP
    elif (self._value.startswith("snmp://")):
      self._parseValue_protocol()
      if (self.port is None):
        self.port = 161
      else:
        self.port = int(self.port)
      self._type = ifSNMP
    elif (re.match(regexUART, self._value)):
      self._parseValue_UART()
    elif (re.match(regexVISA, self._value)):
      self._parseValue_VISA()
    elif (re.match(regexSocket, self._value)):
      self._parseValue_socket()
    else:
      msg = "UNRECOGNIZED {} value: {}".format(self.__class__.__name__, self._value)
      logger.error(msg)
      raise CommException(msg)

class CommInterface(object):
  """Communication interface wrapper.
  Creates a single class for connections over:
    - telnet/socket
    - UART
    - SSH
    - SNMP
    - HTTP
    - FTP
  """

  def __init__(self, strConnInfo=None):
    self._connInfo = None
    self._conn = None
    self._sshtr = None

    self.interBytePause = 0.0
    self.bCertVerify = True

    if (strConnInfo is not None):
      self.setInfo(strConnInfo)

  @property
  def connection(self):
    return self._conn

  @property
  def info(self):
    return self._connInfo

  @property
  def type(self):
    return self._connInfo.type

  @property
  def timeout(self):
    return self._connInfo.timeout

  @timeout.setter
  def timeout(self, val):
    if (isinstance(val, int)):
      self._connInfo.timeout = val

  @property
  def eos(self):
    return self._connInfo.eos

  @eos.setter
  def eos(self, val):
    if (isinstance(val, str)):
      self._connInfo.eos = val

  @property
  def eom(self):
    return self._connInfo.eom

  @eom.setter
  def eom(self, val):
    if (isinstance(val, str)):
      self._connInfo.eom = val

  @property
  def eom_cnt(self):
    return self._connInfo.eom_cnt

  @eom_cnt.setter
  def eom_cnt(self, val):
    if (isinstance(val, int) and (val > 0)):
      self._connInfo.eom_cnt = val

  def __str__(self):
    return str(self._connInfo)

  def setInfo(self, val):
    if (isinstance(val, CommInterfaceInfo)):
      self._connInfo = val
    else:
      self._connInfo = CommInterfaceInfo(val)

  def _open_SSH(self):
    import paramiko

    logger.info("{} connection {}".format(self._connInfo.type.upper(), self._connInfo.INFO))
    try:
      self._sshtr = paramiko.Transport((self._connInfo.host, self._connInfo.port))
      self._sshtr.connect(username=self._connInfo.user, password=self._connInfo.pswd)
      self._conn = self._sshtr.open_session()
      self._conn.settimeout(self._connInfo.timeout)
      #self._conn.set_combine_stderr(True)
      self._conn.get_pty()
      self._conn.invoke_shell()
      #self._conn.recv(2048)  # get whatever the ssh server outputs at first
    except (socket.error, paramiko.AuthenticationException, paramiko.SSHException,
            paramiko.ssh_exception.SSHException) as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)

  def _open_Telnet(self):
    import telnetlib

    logger.info("{} connection {}".format(self._connInfo.type.upper(), self._connInfo.INFO))
    try:
      self._conn = telnetlib.Telnet(self._connInfo.host, self._connInfo.port,
                                    self._connInfo.timeout)
    except socket.timeout as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommTimeoutException(msg)
    except socket.error as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)
    except ConnectionRefusedError as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)

  def _open_Socket(self):
    logger.info("{} connection {}".format(self._connInfo.type.upper(), self._connInfo.INFO))
    self._conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self._conn.settimeout(self._connInfo.timeout)
    try:
      self._conn.connect((self._connInfo.host, self._connInfo.port))
    except socket.timeout as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommTimeoutException(msg)
    except socket.gaierror as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)
    except Exception as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)

  def _open_FTP(self):
    import ftplib
    logger.info("{} connection {}".format(self._connInfo.type.upper(), self._connInfo.INFO))

    self._conn = ftplib.FTP(timeout=self._connInfo.timeout)
    try:
      self._conn.connect(self._connInfo.host, self._connInfo.port)
    except socket.timeout as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommTimeoutException(msg)
    except Exception as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)

    if (self._connInfo.user is None):
      self._conn.login()
    else:
      self._conn.login(self._connInfo.user, self._connInfo.pswd)
    logger.info(self._conn.getwelcome())

  def _open_FTPS(self):
    import ftplib
    logger.info("{} connection {}".format(self._connInfo.type.upper(), self._connInfo.INFO))

    self._conn = ftplib.FTP_TLS(timeout=self._connInfo.timeout)
    try:
      self._conn.connect(self._connInfo.host, self._connInfo.port)
    except socket.timeout as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommTimeoutException(msg)
    except Exception as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)

    if (self._connInfo.user is None):
      self._conn.login()
    else:
      self._conn.login(self._connInfo.user, self._connInfo.pswd)
    self._conn.prot_p()
    logger.info(self._conn.getwelcome())

  def _open_SNMP(self):
    logger.info("{} connection {}".format(self._connInfo.type.upper(), self._connInfo.INFO))
    self._conn = True

  def _open_HTTP(self):
    logger.info("{} connection {}".format(self._connInfo.type.upper(), self._connInfo.INFO))
    self._conn = True

  def _open_UART(self):
    import serial

    #logger.info("platform: {}; device: {}".format(sys.platform, self._connInfo.dev))
    try:
      if (sys.platform.startswith("win")):
        if (not self._connInfo.dev.startswith("COM")):
          msg = "incorrect COM port: {}".format(self._connInfo.dev)
          logger.error(msg)
          raise CommException(msg)
        try:
          com = int(self._connInfo.dev[3:])
          if (com <= 0):
            msg = "incorrect COM port: {}".format(self._connInfo.dev)
            logger.error(msg)
            raise CommException(msg)
          logger.info("{} connection {}".format(self._connInfo.type.upper(),
                                                self._connInfo.INFO))
          self._conn = serial.Serial(self._connInfo.dev,
                                     self._connInfo.speed,
                                     timeout=self._connInfo.timeout,
                                     writeTimeout=(4 * self._connInfo.timeout))
          self._conn.set_buffer_size(rx_size=65536)  # increase RX buffer size
        except ValueError:
          msg = "incorrect COM port: {}".format(self._connInfo.dev)
          logger.error(msg)
          raise CommException(msg)
      elif (sys.platform.startswith("linux")):
        if (not os.path.exists(self._connInfo.dev)):
          msg = "incorrect device: {}".format(self._connInfo.dev)
          logger.error(msg)
          raise CommException(msg)
        logger.info("{} connection {}".format(self._connInfo.type.upper(),
                                              self._connInfo.INFO))
        self._conn = serial.Serial(self._connInfo.dev,
                                   self._connInfo.speed,
                                   timeout=self._connInfo.timeout)
    except serial.SerialException as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)
    except Exception as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)

  def _open_VISA(self):
    if (sys.version_info[0] == 2):
      import visa
    else:
      import pyvisa as visa
    logger.info("{} connection {}".format(self._connInfo.type.upper(), self._connInfo.INFO))

    rm = visa.ResourceManager()
    self._conn = rm.open_resource(self._connInfo.URL)
    self._conn.timeout = (self._connInfo.timeout * 1000)  # ms

  def open(self):
    if (self._connInfo.type == ifSSH):
      self._open_SSH()
    elif (self._connInfo.type == ifTelnet):
      self._open_Telnet()
    elif (self._connInfo.type == ifSocket):
      self._open_Socket()
    elif (self._connInfo.type == ifFTP):
      self._open_FTP()
    elif (self._connInfo.type == ifFTPS):
      self._open_FTPS()
    elif (self._connInfo.type == ifSNMP):
      self._open_SNMP()
    elif (self._connInfo.type in (ifHTTP, ifHTTPS)):
      self._open_HTTP()
    elif (self._connInfo.type == ifUART):
      self._open_UART()
    elif (self._connInfo.type == ifVISA):
      self._open_VISA()
    else:
      msg = "CANNOT start CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

  def _close_SSH(self):
    if (self._conn is None):
      return

    self._conn.close()
    self._sshtr.close()
    logger.info("SSH connection closed")

  def _close_Telnet(self):
    if (self._conn is None):
      return

    self._conn.close()
    logger.info("TELNET connection closed")

  def _close_Socket(self):
    if (self._conn is None):
      return

    self._conn.close()
    logger.info("SOCKET connection closed")

  def _close_FTP(self):
    if (self._conn is None):
      return

    self._conn.quit()
    logger.info("FTP connection closed")

  def _close_FTPS(self):
    if (self._conn is None):
      return

    self._conn.quit()
    logger.info("FTPS connection closed")

  def _close_SNMP(self):
    self._conn = None
    logger.info("SNMP connection closed")

  def _close_HTTP(self):
    self._conn = None
    logger.info("HTTP connection closed")

  def _close_UART(self):
    if (self._conn is None):
      return

    self._conn.close()
    logger.info("UART connection closed")

  def _close_VISA(self):
    self._conn.close()
    logger.info("{} connection closed".format(self._connInfo.type.upper()))

  def close(self):
    if (self._conn is None):
      return

    if (self._connInfo.type == ifSSH):
      self._close_SSH()
    elif (self._connInfo.type == ifTelnet):
      self._close_Telnet()
    elif (self._connInfo.type == ifSocket):
      self._close_Socket()
    elif (self._connInfo.type == ifFTP):
      self._close_FTP()
    elif (self._connInfo.type == ifFTPS):
      self._close_FTPS()
    elif (self._connInfo.type == ifSNMP):
      self._close_SNMP()
    elif (self._connInfo.type in (ifHTTP, ifHTTPS)):
      self._close_HTTP()
    elif (self._connInfo.type == ifUART):
      self._close_UART()
    elif (self._connInfo.type == ifVISA):
      self._close_VISA()
    else:
      msg = "CANNOT stop CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

    self._conn = None

  def isActive(self):
    if (self._conn is None):
      return False
    else:
      return True

  def cleanupReply(self, msg, reply):
    tmp = [t for t in reply if (t != msg)]  # remove echo
    if (self.type == ifSSH):
      tmp = [t for t in tmp
             if (re.match(r"^.*@.*:.*[#$] $", t) is None)]  # remove linux prompt
    return tmp

  def _readRaw_SSH(self, strEOM, TOmultiplier=1):
    if (not isinstance(strEOM, str)):
      raise NotImplementedError  # this should not happen

    tmp_timeout = (TOmultiplier * self._connInfo.timeout)
    reply = ""

    tlim = datetime.timedelta(seconds=tmp_timeout)
    logcomms.info("{}:wait {}x'{}' for {}".format(
        self._connInfo.DBG, self._connInfo.eom_cnt,
        pyauto_base.misc.convertStringNonPrint(strEOM), tlim))
    t0 = datetime.datetime.now()
    dt = datetime.datetime.now() - t0
    while (dt < tlim):
      if (self._conn.recv_ready()):
        tmp = self._conn.recv(2048)
        logcomms.info("{}:({: >4})>>'{}'".format(
            self._connInfo.DBG, len(tmp), pyauto_base.misc.convertStringNonPrint(tmp)))
        #reply = (reply + tmp.decode("utf-8"))
        reply += tmp.decode("utf-8")
        time.sleep(1e-3)  # TODO: this speeds the execution somehow ????
      # yapf: disable
      #if ((strEOM != "") and (reply.endswith(strEOM))):
      if ((strEOM != "") and (reply.endswith(strEOM)) and (reply.count(strEOM) == self._connInfo.eom_cnt)):
        break
      # yapf: enable
      dt = datetime.datetime.now() - t0
    if ((dt > tlim) and (strEOM != "")):
      msg = "{}: timeout {}".format(self, dt)
      logger.error(msg)
      #logger.error(reply)
      raise CommTimeoutException(msg)

    #reply = re.sub(r"\x1b\[[0-9]+;[0-9]+H", "\n", reply)  # needed for windows SSH
    #reply = reply.replace(" \r", "")  # sometimes ssh seems to break lines with a " \r"
    reply = reply.splitlines()
    #reply = [t.strip() for t in reply]  # remove leading/trailing spaces
    reply = [t for t in reply if (t != "")]  # remove empty lines
    return reply

  def _readRaw_Telnet(self, strEOM, TOmultiplier=1):
    if (not isinstance(strEOM, str)):
      raise NotImplementedError  # this should not happen

    tmp_timeout = (TOmultiplier * self._connInfo.timeout)
    reply = ""

    tlim = datetime.timedelta(seconds=tmp_timeout)
    logcomms.info("{}:wait '{}' for {}".format(
        self._connInfo.DBG, pyauto_base.misc.convertStringNonPrint(strEOM), tlim))
    t0 = datetime.datetime.now()
    try:
      if (strEOM == ""):
        # read_until seems to require a character so we choose one that hopefully will not be returned
        reply = self._conn.read_until("\x7F".encode("utf-8"), tmp_timeout)
      else:
        reply = self._conn.read_until(strEOM.encode("utf-8"), tmp_timeout)
    except socket.error as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)
    dt = datetime.datetime.now() - t0

    logcomms.info("{}>>'{}'".format(self._connInfo.DBG,
                                    pyauto_base.misc.convertStringNonPrint(reply)))
    #logcomms.info("{}:time {}".format(self._connInfo.DBG, dt))
    if ((dt > tlim) and (strEOM != "")):
      msg = "{}: timeout {}".format(self, dt)
      logger.error(msg)
      #logger.error(reply)
      raise CommTimeoutException(msg)

    reply = reply.decode("utf-8")
    reply = reply.splitlines()
    #reply = [t.strip() for t in reply]  # remove leading/trailing spaces
    reply = [t for t in reply if (t != "")]  # remove empty lines
    return reply

  def _readRaw_Socket(self, strEOM, TOmultiplier=1, bBytes=False):
    if (not isinstance(strEOM, str)):
      raise NotImplementedError  # this should not happen

    tmp_timeout = (TOmultiplier * self._connInfo.timeout)
    buff = bytearray()
    reply = ""

    tlim = datetime.timedelta(seconds=tmp_timeout)
    logcomms.info("{}:wait '{}' for {}".format(
        self._connInfo.DBG, pyauto_base.misc.convertStringNonPrint(strEOM), tlim))
    t0 = datetime.datetime.now()
    dt = datetime.datetime.now() - t0
    while (dt < tlim):
      dt = datetime.datetime.now() - t0
      try:
        buff += self._conn.recv(1024)
      except socket.timeout as ex:
        #msg = "{}: {}".format(self, ex)
        #logger.error(msg, exc_info=True)
        #raise CommTimeoutException(msg)
        pass
      except socket.error as ex:
        msg = "{}: {}".format(self, ex)
        logger.error(msg, exc_info=True)
        raise CommException(msg)
      if (len(buff) == 0):
        continue
      reply = buff.decode("utf-8", errors="ignore")
      if ((strEOM != "") and (reply.endswith(strEOM))):
        break

    if (bBytes):
      return buff

    logcomms.info("{}>>'{}'".format(self._connInfo.DBG,
                                    pyauto_base.misc.convertStringNonPrint(reply)))
    #logcomms.info("{}:time {}".format(self._connInfo.DBG, dt))
    if ((dt > tlim) and (strEOM != "") and (not reply.endswith(strEOM))):
      msg = "{}: timeout {}".format(self, dt)
      logger.error(msg)
      #logger.error(reply)
      raise CommTimeoutException(msg)

    reply = reply.splitlines()
    #reply = [t.strip() for t in reply]  # remove leading/trailing spaces
    reply = [t for t in reply if (t != "")]  # remove empty lines
    return reply

  def _readRaw_UART(self, strEOM, TOmultiplier=1, bBytes=False):
    import serial

    if (not isinstance(strEOM, str)):
      raise NotImplementedError  # this should not happen

    tmp_timeout = (TOmultiplier * self._connInfo.timeout)
    buff = bytearray()
    reply = ""

    tlim = datetime.timedelta(seconds=tmp_timeout)
    logcomms.info("{}:wait '{}' for {}".format(
        self._connInfo.DBG, pyauto_base.misc.convertStringNonPrint(strEOM), tlim))
    t0 = datetime.datetime.now()
    dt = datetime.datetime.now() - t0
    while (dt < tlim):
      dt = datetime.datetime.now() - t0
      # this is annoying, but have to support older pyserial
      if (not serial.VERSION.startswith("3")):
        while (self._conn.inWaiting() > 0):
          buff += bytes(self._conn.read())
      else:
        while (self._conn.in_waiting > 0):
          buff += self._conn.read()
      if (len(buff) == 0):
        continue
      reply = buff.decode("utf-8", errors="ignore")
      logcomms.info("{}:({: >4})>>'{}'".format(
          self._connInfo.DBG, len(reply), pyauto_base.misc.convertStringNonPrint(reply)))
      if ((strEOM != "") and (reply.endswith(strEOM))):
        break

    if (bBytes):
      return buff

    logcomms.info("{}:time {}".format(self._connInfo.DBG, dt))
    if ((dt > tlim) and (strEOM != "")):
      msg = "{}: timeout {}".format(self, dt)
      logger.error(msg)
      #logger.error(reply)
      raise CommTimeoutException(msg)

    reply = reply.splitlines()
    #reply = [t.strip() for t in reply]  # remove leading/trailing spaces
    reply = [t for t in reply if (t != "")]  # remove empty lines
    return reply

  def readRaw(self, strEOM, TOmultiplier=1, bBytes=False):
    """
    Reads until strEOM is found or until timeout expires.
    :param strEOM: string
    :param TOmultiplier: timeout multiplier
    :param bBytes: returns bytes instead of doing some basic string clean-up
    :return: list of strings (ideally lines received)
    """
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return []

    if (self._connInfo.type == ifSSH):
      reply = self._readRaw_SSH(strEOM, TOmultiplier)
    elif (self._connInfo.type == ifTelnet):
      reply = self._readRaw_Telnet(strEOM, TOmultiplier)
    elif (self._connInfo.type == ifSocket):
      reply = self._readRaw_Socket(strEOM, TOmultiplier, bBytes=bBytes)
    elif (self._connInfo.type == ifUART):
      reply = self._readRaw_UART(strEOM, TOmultiplier, bBytes=bBytes)
    else:
      msg = "CANNOT read from CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

    return reply

  def read(self, TOmultiplier=1):
    return self.readRaw(self.eom, TOmultiplier=TOmultiplier)

  def _writeRaw_SSH(self, strCmd, TOmultiplier=1):
    logcomms.info("{}<< {}".format(self._connInfo.DBG,
                                   pyauto_base.misc.convertStringNonPrint(strCmd)))

    #t0 = datetime.datetime.now()
    self._conn.settimeout(TOmultiplier * self._connInfo.timeout)
    self._conn.send(strCmd)
    self._conn.settimeout(self._connInfo.timeout)
    #logcomms.info("{}:time {}".format(self._connInfo.DBG, datetime.datetime.now() - t0))

  def _writeRaw_Telnet(self, strCmd, TOmultiplier=1):
    logcomms.info("{}<< {}".format(self._connInfo.DBG,
                                   pyauto_base.misc.convertStringNonPrint(strCmd)))

    #t0 = datetime.datetime.now()
    try:
      self._conn.write(strCmd.encode("utf-8"))
    except socket.error as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)
    #logcomms.info("{}:time {}".format(self._connInfo.DBG, datetime.datetime.now() - t0))

  def _writeRaw_Socket(self, strCmd, TOmultiplier=1):
    logcomms.info("{}<< {}".format(self._connInfo.DBG,
                                   pyauto_base.misc.convertStringNonPrint(strCmd)))

    #t0 = datetime.datetime.now()
    try:
      if (isinstance(strCmd, bytearray)):
        self._conn.sendall(strCmd)
      else:
        self._conn.sendall(strCmd.encode("utf-8"))
    except socket.error as ex:
      msg = "{}: {}".format(self, ex)
      logger.error(msg, exc_info=True)
      raise CommException(msg)
    #logcomms.info("{}:time {}".format(self._connInfo.DBG, datetime.datetime.now() - t0))

  def _writeRaw_UART(self, strCmd, TOmultiplier=1):
    logcomms.info("{}:({: >4})<< {}".format(self._connInfo.DBG, len(strCmd),
                                            pyauto_base.misc.convertStringNonPrint(strCmd)))

    #t0 = datetime.datetime.now()
    if (self.interBytePause == 0.0):
      self._conn.write(strCmd.encode("utf-8"))
    else:
      for b in strCmd.encode("utf-8"):
        self._conn.write((b).to_bytes(1, byteorder="big"))
        time.sleep(self.interBytePause)
    self._conn.flush()
    #logcomms.info("{}:time {}".format(self._connInfo.DBG, datetime.datetime.now() - t0))

  def _writeRaw_VISA(self, strCmd, TOmultiplier=1):
    to = self._conn.timeout
    if (TOmultiplier > 1):
      self._conn.timeout = (TOmultiplier * to)

    logcomms.info("{}<< {}".format(self._connInfo.DBG,
                                   pyauto_base.misc.convertStringNonPrint(strCmd)))
    self._conn.write_raw(strCmd.encode("utf-8"))

    if (TOmultiplier > 1):
      self._conn.timeout = to

  def writeRaw(self, strCmd, TOmultiplier=1):
    """
    Writes strCmd.
    :param strCmd: string
    :param TOmultiplier:  timeout multiplier
    """
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return []

    if (self._connInfo.type == ifSSH):
      self._writeRaw_SSH(strCmd, TOmultiplier)
    elif (self._connInfo.type == ifTelnet):
      self._writeRaw_Telnet(strCmd, TOmultiplier)
    elif (self._connInfo.type == ifSocket):
      self._writeRaw_Socket(strCmd, TOmultiplier)
    elif (self._connInfo.type == ifUART):
      self._writeRaw_UART(strCmd, TOmultiplier)
    elif (self._connInfo.type == ifVISA):
      self._writeRaw_VISA(strCmd, TOmultiplier)
    else:
      msg = "CANNOT write on CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

  def write(self, strCmd):
    if (isinstance(strCmd, bytearray)):
      return self.writeRaw(strCmd)
    else:
      return self.writeRaw(strCmd + self.eos)

  def _communicate_SSH(self, strCmd, strEOM, TOmultiplier=1):
    if (len(strCmd) > 0):
      self._writeRaw_SSH(strCmd, TOmultiplier)
    if (strEOM is None):
      return []
    return self._readRaw_SSH(strEOM, TOmultiplier)

  def _communicate_Telnet(self, strCmd, strEOM, TOmultiplier=1):
    if (len(strCmd) > 0):
      self._writeRaw_Telnet(strCmd, TOmultiplier)
    if (strEOM is None):
      return []
    return self._readRaw_Telnet(strEOM, TOmultiplier)

  def _communicate_Socket(self, strCmd, strEOM, TOmultiplier=1):
    if (len(strCmd) > 0):
      self._writeRaw_Socket(strCmd, TOmultiplier)
    if (strEOM is None):
      return []
    return self._readRaw_Socket(strEOM, TOmultiplier)

  def _communicate_UART(self, strCmd, strEOM, TOmultiplier=1):
    if (len(strCmd) > 0):
      self._writeRaw_UART(strCmd, TOmultiplier)
    if (strEOM is None):
      return []
    return self._readRaw_UART(strEOM, TOmultiplier)

  def communicate(self, strCmd, strEOM="", TOmultiplier=1):
    """
    Sends a command and reads until strEOM is found or until timeout expires.
    If strCmd is empty it does not send anything.
    If strEOM is None it does not wait for reply.
    If strEOM is "" it will wait for timeout.
    :param strCmd: string
    :param strEOM: string
    :param TOmultiplier: timeout multiplier
    :return: list of strings (ideally lines received)
    """
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return []

    if (self._connInfo.type == ifSSH):
      reply = self._communicate_SSH(strCmd, strEOM, TOmultiplier)
    elif (self._connInfo.type == ifTelnet):
      reply = self._communicate_Telnet(strCmd, strEOM, TOmultiplier)
    elif (self._connInfo.type == ifSocket):
      reply = self._communicate_Socket(strCmd, strEOM, TOmultiplier)
    elif (self._connInfo.type == ifUART):
      reply = self._communicate_UART(strCmd, strEOM, TOmultiplier)
    else:
      msg = "CANNOT communicate on CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

    #if(len(reply) > 0):
    #  res = [t.strip("\n\r") for t in reply]
    #  res = [t for t in res if(t != "")] # strip empty list entries
    #  return res
    return reply

  def _query_VISA(self, strCmd, TOmultiplier=1):
    to = self._conn.timeout
    if (TOmultiplier > 1):
      self._conn.timeout = (TOmultiplier * to)

    logcomms.info("{}<< {}".format(self._connInfo.DBG,
                                   pyauto_base.misc.convertStringNonPrint(strCmd)))
    logcomms.info("{}:wait for {}".format(self._connInfo.DBG, self._conn.timeout))
    reply = self._conn.query(strCmd)
    logcomms.info("{}:({: >4})>>'{}'".format(self._connInfo.DBG, len(reply),
                                             pyauto_base.misc.convertStringNonPrint(reply)))

    if (TOmultiplier > 1):
      self._conn.timeout = to

    reply = reply.splitlines()
    #reply = [t.strip() for t in reply]  # remove leading/trailing spaces
    reply = [t for t in reply if (t != "")]  # remove empty lines
    return reply

  def query(self, strCmd, TOmultiplier=1):
    if (self._connInfo.type == ifVISA):
      return self._query_VISA(strCmd, TOmultiplier)
    else:
      return self.communicate((strCmd + self.eos),
                              strEOM=self.eom,
                              TOmultiplier=TOmultiplier)

  def _get_HTTP(self, **kwargs):
    """
    HTTP GET method
    :param kwargs: url=?, params=?, data=?, headers=?, auth=?, TOmultiplier=?, bJson=False
    """
    if ("TOmultiplier" in kwargs.keys()):
      TOmultiplier = int(kwargs["TOmultiplier"])
    else:
      TOmultiplier = 1
    tmp_timeout = (TOmultiplier * self._connInfo.timeout)

    if ("url" in kwargs.keys()):
      if (kwargs["url"].startswith("http://") or kwargs["url"].startswith("https://")):
        url = kwargs["url"]
      else:
        url = self._connInfo.URL + kwargs["url"]
    else:
      url = self._connInfo.URL
    if ("params" in kwargs.keys()):
      params = kwargs["params"]
    else:
      params = None
    if ("data" in kwargs.keys()):
      data = kwargs["data"]
    else:
      data = None
    if ("headers" in kwargs.keys()):
      headers = kwargs["headers"]
    else:
      headers = None
    if ("auth" in kwargs.keys()):
      auth = kwargs["auth"]
    else:
      auth = None

    try:
      reply = requests.get(
          url,
          params=params,
          data=data,
          headers=headers,
          auth=auth,
          timeout=tmp_timeout,
      )
    except requests.ConnectTimeout as ex:
      raise CommTimeoutException(ex)
    except requests.ReadTimeout as ex:
      raise CommTimeoutException(ex)
    except requests.Timeout as ex:
      raise CommTimeoutException(ex)

    if (reply.status_code != 200):
      raise CommTimeoutException(reply.text)

    if ("bJson" in kwargs.keys()):
      return reply.json()
    elif ("bBytes" in kwargs.keys()):
      return bytes(reply.content)
    else:
      return reply.text

  def _get_SNMP(self, **kwargs):
    """
    get "key" from SNMP server
    :param kwargs: key=?, TOmultiplier=?
    """
    if ("key" not in kwargs.keys()):
      return
    if (pyauto_base.misc.isEmptyString(kwargs["key"])):
      return

    import pysnmp.hlapi

    if ("TOmultiplier" in kwargs.keys()):
      TOmultiplier = int(kwargs["TOmultiplier"])
    else:
      TOmultiplier = 1
    tmp_timeout = (TOmultiplier * self._connInfo.timeout)
    logcomms.info("{}<< {}".format(self._connInfo.DBG, kwargs["key"]))
    errInd, errSts, errIdx, varBinds = next(
        pysnmp.hlapi.getCmd(
            pysnmp.hlapi.SnmpEngine(),
            pysnmp.hlapi.CommunityData(self._connInfo.SNMP_ro),
            pysnmp.hlapi.UdpTransportTarget((self._connInfo.host, self._connInfo.port),
                                            timeout=tmp_timeout),
            pysnmp.hlapi.ContextData(),
            pysnmp.hlapi.ObjectType(pysnmp.hlapi.ObjectIdentity(kwargs["key"])),
        ))
    if (errInd is not None):
      msg = str(errInd)
      logger.error(msg)
      raise CommException(msg)
    if (errSts != 0):
      msg = errSts.prettyPrint()
      logger.error(msg)
      raise CommException(msg)

    reply = []
    for n, v in varBinds:
      reply.append(n.prettyPrint())
      reply.append(v.prettyPrint())
    logcomms.info("{}>>'{}'".format(self._connInfo.DBG, reply))
    #logcomms.info("SNMP GET <>\n{}: {}".format(n.prettyPrint(), v.prettyPrint()))
    return reply

  def get(self, **kwargs):
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return

    if (self._connInfo.type == ifSNMP):
      return self._get_SNMP(**kwargs)
    elif (self._connInfo.type in (ifHTTP, ifHTTPS)):
      return self._get_HTTP(**kwargs)
    else:
      msg = "CANNOT get from CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

  def _set_SNMP(self, **kwargs):
    """
    set "key"="val" to SNMP server
    :param kwargs: key=?, val=?, TOmultiplier=?
    """
    if ("key" not in kwargs.keys()):
      return
    if (pyauto_base.misc.isEmptyString(kwargs["key"])):
      return
    if ("val" not in kwargs.keys()):
      return
    if (pyauto_base.misc.isEmptyString(kwargs["val"])):
      return

    import pysnmp.hlapi

    if ("TOmultiplier" in kwargs.keys()):
      TOmultiplier = int(kwargs["TOmultiplier"])
    else:
      TOmultiplier = 1
    tmp_timeout = (TOmultiplier * self._connInfo.timeout)
    logcomms.info("{}<< {} = {}".format(self._connInfo.DBG, kwargs["key"], kwargs["val"]))
    #logger.info("SNMP SET <<\n{} [{}] {}".format(strKey, strVal))
    errInd, errSts, errIdx, varBinds = next(
        pysnmp.hlapi.setCmd(
            pysnmp.hlapi.SnmpEngine(),
            pysnmp.hlapi.CommunityData(self._connInfo.SNMP_rw),
            pysnmp.hlapi.UdpTransportTarget((self._connInfo.host, self._connInfo.port),
                                            timeout=tmp_timeout),
            pysnmp.hlapi.ContextData(),
            pysnmp.hlapi.ObjectType(pysnmp.hlapi.ObjectIdentity(kwargs["key"]),
                                    kwargs["val"]),
        ))
    if (errInd is not None):
      logger.error(errInd)
      return
    if (errSts != 0):
      logger.error(errSts.prettyPrint())
      return

  def set(self, **kwargs):
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return

    if (self._connInfo.type == ifSNMP):
      return self._set_SNMP(**kwargs)
    else:
      msg = "CANNOT set on CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

  def _post_HTTP(self, **kwargs):
    """
    HTTP POST method
    :param kwargs: url=?, params=?, data=?, headers=?, auth=?, TOmultiplier=?, bJson=False
    """
    if ("TOmultiplier" in kwargs.keys()):
      TOmultiplier = int(kwargs["TOmultiplier"])
    else:
      TOmultiplier = 1
    tmp_timeout = (TOmultiplier * self._connInfo.timeout)

    if ("url" in kwargs.keys()):
      if (kwargs["url"].startswith("http://") or kwargs["url"].startswith("https://")):
        url = kwargs["url"]
      else:
        url = self._connInfo.URL + kwargs["url"]
    else:
      url = self._connInfo.URL
    if ("params" in kwargs.keys()):
      params = kwargs["params"]
    else:
      params = None
    if ("data" in kwargs.keys()):
      data = kwargs["data"]
    else:
      data = None
    if ("headers" in kwargs.keys()):
      headers = kwargs["headers"]
    else:
      headers = None
    if ("auth" in kwargs.keys()):
      auth = kwargs["auth"]
    else:
      auth = None

    try:
      reply = requests.post(
          url,
          params=params,
          headers=headers,
          data=data,
          auth=auth,
          timeout=tmp_timeout,
      )
    except requests.ConnectTimeout as ex:
      raise CommTimeoutException(ex)
    except requests.ReadTimeout as ex:
      raise CommTimeoutException(ex)
    except requests.Timeout as ex:
      raise CommTimeoutException(ex)

    if (reply.status_code != 200):
      raise CommTimeoutException(reply.text)

    if ("bJson" in kwargs.keys()):
      return reply.json()
    else:
      return reply.text

  def post(self, **kwargs):
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return

    if (self._connInfo.type in (ifHTTP, ifHTTPS)):
      return self._post_HTTP(**kwargs)
    else:
      msg = "CANNOT post on CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

  def _put_HTTP(self, **kwargs):
    """
    HTTP PUT method
    :param kwargs: url=?, params=?, data=?, headers=?, auth=?, TOmultiplier=?, bJson=False
    """
    if ("TOmultiplier" in kwargs.keys()):
      TOmultiplier = int(kwargs["TOmultiplier"])
    else:
      TOmultiplier = 1
    tmp_timeout = (TOmultiplier * self._connInfo.timeout)

    if ("url" in kwargs.keys()):
      if (kwargs["url"].startswith("http://") or kwargs["url"].startswith("https://")):
        url = kwargs["url"]
      else:
        url = self._connInfo.URL + kwargs["url"]
    else:
      url = self._connInfo.URL
    if ("params" in kwargs.keys()):
      params = kwargs["params"]
    else:
      params = None
    if ("data" in kwargs.keys()):
      data = kwargs["data"]
    else:
      data = None
    if ("headers" in kwargs.keys()):
      headers = kwargs["headers"]
    else:
      headers = None
    if ("auth" in kwargs.keys()):
      auth = kwargs["auth"]
    else:
      auth = None

    try:
      reply = requests.put(
          url,
          params=params,
          headers=headers,
          data=data,
          auth=auth,
          timeout=tmp_timeout,
      )
    except requests.ConnectTimeout as ex:
      raise CommTimeoutException(ex)
    except requests.ReadTimeout as ex:
      raise CommTimeoutException(ex)
    except requests.Timeout as ex:
      raise CommTimeoutException(ex)

    if (reply.status_code != 200):
      raise CommTimeoutException(reply.text)

    if ("bJson" in kwargs.keys()):
      return reply.json()
    else:
      return reply.text

  def put(self, **kwargs):
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return

    if (self._connInfo.type in (ifHTTP, ifHTTPS)):
      return self._put_HTTP(**kwargs)
    else:
      msg = "CANNOT put on CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

  def _putFile_SSH(self, lclPath, rmtPath, TOmultiplier=1):
    if (not pyauto_base.fs.chkPath_File(lclPath, bDie=False)):
      return

    logger.info("sftp: {} -> {}".format(lclPath, rmtPath))
    sftp = self._sshtr.open_sftp_client()
    res = sftp.put(lclPath, rmtPath)
    logger.info("sftp: {} byte(s) transferred".format(res.st_size))

  def _putFile_FTP(self, lclPath, rmtPath, TOmultiplier=1):
    if (not pyauto_base.fs.chkPath_File(lclPath, bDie=False)):
      return

    logger.info("ftp: {} -> {}".format(lclPath, rmtPath))
    with open(lclPath, "rb") as lclFile:
      self._conn.storbinary("STOR {}".format(rmtPath), lclFile)
    logger.info("sftp: {} byte(s) transferred".format(os.stat(lclPath).st_size))

  def putFile(self, lclPath, rmtPath, TOmultiplier=1):
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return

    if (self._connInfo.type == ifSSH):
      return self._putFile_SSH(lclPath, rmtPath, TOmultiplier)
    elif (self._connInfo.type in (ifFTP, ifFTPS)):
      return self._putFile_FTP(lclPath, rmtPath, TOmultiplier)
    else:
      msg = "CANNOT putFile on CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

  def _getFile_SSH(self, rmtPath, lclPath, TOmultiplier=1):
    logger.info("sftp: {} <- {}".format(lclPath, rmtPath))
    sftp = self._sshtr.open_sftp_client()
    sftp.get(rmtPath, lclPath)

    if (not pyauto_base.fs.chkPath_File(lclPath, bDie=False)):
      return
    logger.info("sftp: {} byte(s) transferred".format(os.stat(lclPath).st_size))

  def _getFile_FTP(self, rmtPath, lclPath, TOmultiplier=1):
    logger.info("ftp: {} <- {}".format(lclPath, rmtPath))
    with open(lclPath, "wb") as lclFile:
      self._conn.retrbinary("RETR {}".format(rmtPath), lclFile.write)

    if (not pyauto_base.fs.chkPath_File(lclPath, bDie=False)):
      return
    logger.info("ftp: {} byte(s) transferred".format(os.stat(lclPath).st_size))

  def getFile(self, rmtPath, lclPath, TOmultiplier=1):
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return

    if (self._connInfo.type == ifSSH):
      return self._getFile_SSH(rmtPath, lclPath, TOmultiplier)
    elif (self._connInfo.type in (ifFTP, ifFTPS)):
      return self._getFile_FTP(rmtPath, lclPath, TOmultiplier)
    else:
      msg = "CANNOT getFile on CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

  def _delFile_SSH(self, rmtPath, lclPath, TOmultiplier=1):
    raise NotImplementedError

  def _delFile_FTP(self, rmtPath, TOmultiplier=1):
    import ftplib

    logger.info("ftp: del {}".format(rmtPath, rmtPath))
    try:
      res = self._conn.delete(rmtPath)
    except ftplib.error_perm as ex:
      logger.warning(ex)

  def delFile(self, rmtPath, TOmultiplier=1):
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return

    if (self._connInfo.type == ifSSH):
      return self._delFile_SSH(rmtPath, TOmultiplier)
    elif (self._connInfo.type in (ifFTP, ifFTPS)):
      return self._delFile_FTP(rmtPath, TOmultiplier)
    else:
      msg = "CANNOT getFile on CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

  def _ls_FTP(self):
    res = self._conn.nlst()
    return res

  def ls(self):
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return

    if (self._connInfo.type in (ifFTP, ifFTPS)):
      return self._ls_FTP()
    else:
      msg = "CANNOT ls on CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

  def _pwd_FTP(self):
    res = self._conn.pwd()
    logger.info("ftp pwd: {}".format(res))
    return res

  def pwd(self):
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return

    if (self._connInfo.type in (ifFTP, ifFTPS)):
      return self._pwd_FTP()
    else:
      msg = "CANNOT pwd on CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)

  def _cwd_FTP(self, rmtPath):
    self._conn.cwd(rmtPath)

  def cwd(self, rmtPath):
    if (self._conn is None):
      logger.error("None connection for CommInterface of type: {}".format(
          self._connInfo.type))
      return

    if (self._connInfo.type in (ifFTP, ifFTPS)):
      return self._cwd_FTP(rmtPath)
    else:
      msg = "CANNOT cwd on CommInterface type: {}".format(self._connInfo.type)
      logger.error(msg)
      raise CommException(msg)
