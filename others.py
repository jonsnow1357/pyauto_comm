#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import requests
import requests.auth
import urllib.parse

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_base.config
import pyauto_comm.web as libWeb

def enableCertVerification():
  libWeb.enableCertVerification()

def disableCertVerification():
  libWeb.disableCertVerification()

class HIBP(libWeb.WebService):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(HIBP, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)
    self._headers = {"user-agent": "python script", "hibp-api-key": self._auth["pass"]}

  def getAllBreaches(self):
    url = "{}/breaches".format(self._endpoint["url"])
    res = requests.get(url)
    libWeb.chkRequestStatus(res)
    return res.json()

  def getBreachedAccount(self, email=None):
    if (email is None):
      return []

    url = "{}/breachedaccount/{}".format(self._endpoint["url"], email)
    res = requests.get(url, headers=self._headers)
    libWeb.chkRequestStatus(res)
    res = [t["Name"] for t in res.json()]
    return res

  def getBreach(self, name=None):
    if (name is None):
      return []

    url = "{}/breach/{}".format(self._endpoint["url"], name)
    res = requests.get(url, headers=self._headers, timeout=4)
    libWeb.chkRequestStatus(res)
    return res.json()

class PhpIpam(libWeb.WebService):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(PhpIpam, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)
    self.token = ""

  def connect(self):
    if (self._auth["type"] != libWeb.authTypeBasic):
      logger.error("UNSUPPORTED auth type: {}".format(self._auth["type"]))
      return

    _url = "{}/user".format(self._endpoint["url"])
    res = requests.post(_url,
                        auth=requests.auth.HTTPBasicAuth(self._auth["user"],
                                                         self._auth["pass"]))
    #print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error("login FAIL")
      return
    reply = res.json()
    if (reply["success"] is not True):
      logger.error("login FAIL")
      return

    logger.info("user: {}".format(self._auth["user"]))
    self.token = reply["data"]["token"]
    #logger.info("token: {}".format(self.token))

  def disconnect(self):
    _url = "{}/user".format(self._endpoint["url"])
    headers = {"token": self.token}
    res = requests.delete(_url, headers=headers)
    #print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error("login FAIL")
      return
    reply = res.json()
    if (reply["success"] is not True):
      logger.error("login FAIL")
      return

  def getSubnets(self):
    _url = "{}/subnets/all".format(self._endpoint["url"])
    headers = {"token": self.token}
    res = requests.get(_url, headers=headers)
    #print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error(res.text)
      return
    reply = res.json()
    if (reply["success"] is not True):
      logger.error("login FAIL")
      return

    lst_subnets = []
    for s in reply["data"]:
      #print("DBG", s)
      lst_subnets.append({"id": s["id"], "subnet": "{}/{}".format(s["subnet"], s["mask"])})
    return lst_subnets

  def getSubnet(self, subnetId):
    _url = "{}/subnets/{}/".format(self._endpoint["url"], subnetId)
    headers = {"token": self.token}
    res = requests.get(_url, headers=headers)
    #print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error(res.text)
      return
    reply = res.json()
    if (reply["success"] is not True):
      logger.error("login FAIL")
      return

    lst_addr = []
    subnet = {
        "cidr": "{}/{}".format(reply["data"]["subnet"], reply["data"]["mask"]),
        "usage": None,
        "addr": lst_addr,
    }

    _url = "{}/subnets/{}/usage".format(self._endpoint["url"], subnetId)
    headers = {"token": self.token}
    res = requests.get(_url, headers=headers)
    #print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error(res.text)
      return
    reply = res.json()
    if (reply["success"] is not True):
      logger.error("login FAIL")
      return

    subnet["usage"] = reply["data"]["Used_percent"]

    _url = "{}/subnets/{}/addresses".format(self._endpoint["url"], subnetId)
    headers = {"token": self.token}
    res = requests.get(_url, headers=headers)
    #print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error(res.text)
      return
    reply = res.json()
    if (reply["success"] is not True):
      logger.error("login FAIL")
      return

    for v in reply["data"]:
      lst_addr.append({
          "id": v["id"],
          "IP": v["ip"],
          "hostname": v["hostname"],
          "desc": v["description"],
          "lastSeen": v["lastSeen"],
      })
    return subnet

  def getIP(self, IPId):
    _url = "{}/addresses/{}".format(self._endpoint["url"], IPId)
    headers = {"token": self.token}
    res = requests.get(_url, headers=headers)
    #print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error(res.text)
      return
    reply = res.json()
    if (reply["success"] is not True):
      logger.error("login FAIL")
      return

    ip = {
        "id": reply["data"]["id"],
        "subnetId": reply["data"]["subnetId"],
        "IP": reply["data"]["ip"],
        "hostname": reply["data"]["hostname"],
        "desc": reply["data"]["description"],
        "lastSeen": reply["data"]["lastSeen"],
    }
    return ip

  def delIP(self, IPId):
    _url = "{}/addresses/{}/ping".format(self._endpoint["url"], IPId)
    headers = {"token": self.token}
    res = requests.get(_url, headers=headers)
    #print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error(res.text)
      return
    reply = res.json()
    if (reply["success"] is not True):
      logger.error("login FAIL")
      return

    if (reply["data"]["result_code"] != "OFFLINE"):
      logger.warning("IP online")
      return

    _url = "{}/addresses/{}".format(self._endpoint["url"], IPId)
    headers = {"token": self.token}
    params = {"remove_dns": 1}
    res = requests.delete(_url, headers=headers, params=params)
    #print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error(res.text)
      return
    reply = res.json()
    if (reply["success"] is not True):
      logger.error("login FAIL")
      return

class RideWithGPS(libWeb.WebService):

  def __init__(self, cfgPath=None, cfgSection=None):
    super(RideWithGPS, self).__init__(cfgPath=cfgPath, cfgSection=cfgSection)
    self._endpoint["ver"] = "2"
    self._headers = {
        "x-rwgps-api-key":
        self._endpoint["key"],
        "x-rwgps-api-version":
        self._endpoint["ver"],
        "Authorization":
        "{} {}".format(self._oauth_token["token_type"], self._oauth_token["access_token"])
    }
    self._user_id = None

  def connect(self):
    _url = "{}/users/current.json".format(self._endpoint["url"])
    res = requests.get(_url, headers=self._headers)
    # print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error("connect FAIL")
      return
    reply = res.json()
    self._user_id = reply["user"]["id"]

    name = reply["user"]["name"]
    place = "{} {} {}".format(reply["user"]["locality"],
                              reply["user"]["administrative_area"],
                              reply["user"]["country_code"])
    n_msg = reply["user"]["num_unread_messages"]
    n_ride = reply["user"]["trips_included_in_totals_count"]
    logger.info("user [{}]: {}, {}, {} ride(s), {} unread msg(s)".format(
        self._user_id, name, place, n_ride, n_msg))

  def getRoutes(self):
    _url = "{}/users/{}/routes.json?offset=0&limit={}".format(self._endpoint["url"],
                                                              self._user_id, 0)
    res = requests.get(_url, headers=self._headers)
    # print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error("connect FAIL")
      return
    route_cnt = res.json()["results_count"]

    _url = "{}/users/{}/routes.json?offset=0&limit={}".format(self._endpoint["url"],
                                                              self._user_id, route_cnt)
    res = requests.get(_url, headers=self._headers)
    # print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error("connect FAIL")
      return
    reply = res.json()

    logger.info("got {}/{} route(s)".format(len(reply["results"]), reply["results_count"]))
    lst_routes = []
    for route in reply["results"]:
      #print("DBG", route)
      # yapf: disable
      dist = round((route["distance"] / 1000.0), 1)
      up = route["elevation_gain"] if (route["elevation_gain"] is None) else int(route["elevation_gain"])
      down = route["elevation_loss"] if (route["elevation_loss"] is None) else int(route["elevation_loss"])
      # yapf: enable
      lst_routes.append({
          "id": route["id"],
          "user_id": route["user_id"],
          "name": route["name"],
          "loc": route["locality"],
          "area": route["administrative_area"],
          "country": route["country_code"],
          "dist": dist,
          "up": up,
          "down": down,
          "max_grade": route["max_grade"],
      })
    return lst_routes

  def getRides(self, cnt, offset=0):
    _url = "{}/users/{}/trips.json?offset={}&limit={}".format(self._endpoint["url"],
                                                              self._user_id, offset, cnt)
    res = requests.get(_url, headers=self._headers)
    # print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error("connect FAIL")
      return
    reply = res.json()

    logger.info("got {}/{} ride(s)".format(len(reply["results"]), reply["results_count"]))
    lst_rides = []
    for ride in reply["results"]:
      #print("DBG", ride)
      # yapf: disable
      dist = round((ride["distance"] / 1000.0), 1)
      up = ride["elevation_gain"] if (ride["elevation_gain"] is None) else int(ride["elevation_gain"])
      down = ride["elevation_loss"] if (ride["elevation_loss"] is None) else int(ride["elevation_loss"])
      avg_speed = round(ride["avg_speed"], 1)
      avg_watts = ride["avg_watts"] if (ride["avg_watts"] is None) else round(ride["avg_watts"])
      max_grade = ride["max_grade"] if (ride["max_grade"] is None) else round(ride["max_grade"], 1)
      # yapf: enable
      lst_rides.append({
          "id": ride["id"],
          "gear_id": ride["gear_id"],
          "name": ride["name"],
          "start": ride["departed_at"],
          "is_gps": ride["is_gps"],
          "dist": dist,
          "up": up,
          "down": down,
          "avg_speed": avg_speed,
          "moving_time": ride["moving_time"],
          "avg_watts": avg_watts,
          "calories": ride["calories"],
          "max_grade": max_grade,
      })
    return lst_rides

  def addRide(self, name, path):
    _url = "{}/trips.json?{}={}&{}={}&{}={}".format(
        self._endpoint["url"],
        urllib.parse.quote("trip[name]"),
        name,
        urllib.parse.quote("trip[description]"),
        "",
        urllib.parse.quote("trip[bad_elevations]"),
        "false",
    )
    #_data = {"trip": {"name": name, "description": "", "bad_elevations": False}}
    res = requests.post(_url, headers=self._headers, files={"file": open(path, "rb")})
    # print("DBG", res.status_code, res.json())
    if (res.status_code != 200):
      logger.error("upload FAIL - {}".format(res.text))
      return
    reply = res.json()
    if (reply["success"] != 1):
      logger.error("upload FAIL")
      return
    task_id = reply["task_id"]

    for _ in range(10):
      _url = "{}/queued_tasks/status.json?ids={}&include_objects=false".format(
          self._endpoint["url"], task_id)
      res = requests.get(_url, headers=self._headers)
      # print("DBG", res.status_code, res.json())
      if (res.status_code != 200):
        logger.error("connect FAIL")
        return
      reply = res.json()["queued_tasks"][0]
      logger.info([reply["status"], reply["response_code"], reply["message"]])
      if (reply["status"] == 1):
        break
