#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
import random

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_comm.IoT as libIoT

def mainApp():
  #libIoT.disableCertVerification()
  serv = libIoT.ThingSpeak()
  #serv.testMode = cliArgs["test"]
  testChannelId = "306999"

  if (cliArgs["action"] == "cfg"):
    logger.info(serv)
  elif (cliArgs["action"] == "info"):
    serv.getChannelList()
    serv.showChannels()
  elif (cliArgs["action"] == "test_clr"):
    serv.getChannelMetadata(testChannelId)
    serv.clearChannel(testChannelId)
  elif (cliArgs["action"] == "test_rd"):
    serv.getChannelMetadata(testChannelId)
    dictFeed = serv.getChannelFeed(testChannelId)

    logger.info(serv.channels[testChannelId])
    logger.info("received {} value(s)".format(len(dictFeed)))
    for v in dictFeed:
      logger.info(v)
  elif (cliArgs["action"] == "test_wr"):
    serv.getChannelMetadata(testChannelId)

    ch = serv.channels[testChannelId]
    logger.info(ch)
    ch.fields = ["Field Label 1"]
    serv.updateChannel(testChannelId)
    serv.updateChannelFeed(testChannelId, [random.randint(0, 100)])
    logger.info(ch)

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "web service"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action",
                      help="action",
                      choices=("cfg", "info", "test_rd", "test_wr", "test_clr"))
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-t", "--test", action="store_true", help="just print")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
